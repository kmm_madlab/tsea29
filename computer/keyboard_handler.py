import datetime
import pygame
import bluetooth_handler as b_h
import json
import logging

"""
All GUI related functionality and keyboard handling.
"""

COORD_UP = [[225, 100], [375, 100], [300, 0]]
COORDS_DOWN = [[225, 300], [375, 300], [300, 400]]
COORDS_RIGHT = [[450, 125], [450, 275], [550, 200]]
COORDS_LEFT = [[150, 125], [150, 275], [50, 200]]

GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLACK = (0, 0, 0)


def init_arrow_window():
    """
    Set start up variables and initizilize the screen to hite background. Fills the screen with buttons.
    :return: (Screen)
    """
    backgroud_colour = (255, 255, 255)
    (width, hight) = (600, 400)

    screen = pygame.display.set_mode((width, hight))
    pygame.display.set_caption(" --- MADLAB Steering Window ---")
    screen.fill(backgroud_colour)

    make_all_buttons_black(screen)

    pygame.display.flip()
    
    return screen


def handle_user_input(b_socket):
    """
    Reads the key bords input from the user and changes the current_command accordingly.
    Updates the current_command by changing the color of the active button.
    :param b_socket:
    :return: (JSON String) always {'walk': ["stop"]}
    """

    screen = init_arrow_window()
    current_command = 'stop'  # Always start in stop
    running = True
    
    while running:  # Do this until user quits
            for event in pygame.event.get():

                if event.type == pygame.QUIT:  # TODO add square in midldle of screen.
                    print("-- Quit steering -- ")
                    running = False
                    break
                elif event.type == pygame.KEYDOWN:  # Any button on the keyboard is pressed.
                    make_all_buttons_black(screen)
                    pygame.display.flip()

                    # Set command depending on what key was pressed
                    if event.key == pygame.K_LEFT:
                        change_color_button(screen, COORDS_LEFT)
                        current_command = "left"
                    elif event.key == pygame.K_RIGHT:
                        change_color_button(screen, COORDS_RIGHT)
                        current_command = "right"
                    elif event.key == pygame.K_UP:
                        change_color_button(screen, COORD_UP)
                        current_command = "forward"
                    elif event.key == pygame.K_DOWN:
                        change_color_button(screen, COORDS_DOWN)
                        current_command = "backward"
                    elif event.key == pygame.K_SPACE:
                        current_command = "stop"
                    elif event.key == (pygame.K_q or pygame.K_ESCAPE):
                        print("-- Quit steering -- ")
                        running = False
                        break

                    pygame.display.flip()

                    # Send command to robot
                    update_current_command(current_command, b_socket)


    pygame.quit()
    return json.dumps({'walk': ["stop"]})


def make_all_buttons_black(screen):
    """
    Draws all arrows on the screen black.
    :param screen: (Screen)
    :return: (None)
    """
    pygame.draw.polygon(screen, BLACK, COORDS_LEFT)  # Left arrow
    pygame.draw.polygon(screen, BLACK, COORDS_RIGHT)   # Right arrow
    pygame.draw.polygon(screen, BLACK, COORDS_DOWN)  # Down arrow
    pygame.draw.polygon(screen, BLACK, COORD_UP)  # Up arrow


def change_color_button(screen, coords):
    """
    Sets the color to red to a arrow on the screen.
    :param screen: (Screen)
    :param coords: (Tuple)
    :return: (None)
    """
    pygame.draw.polygon(screen, RED, coords)  # coords arrow


def update_current_command(command, b_socket):
    """
    Converts commmand to JSON String and sends it to the raspberry. Response from raspberry is sent to logging.
    :param command: (String)
    :param b_socket: (BluetoothSocket)
    :return: (None)
    """
    message = json.dumps({'walk': [command]})
    response = b_h.send_message(message, b_socket)
    handle_response(response)


def handle_response(response):
    """
    Logs status and queued commands to file and saves it in a appropriate format.
    :param response: (JSON String)
    :return: (None)
    """
    now = datetime.datetime.now()

    response = json.loads(response)
    status = "\n[DATE: %s/%s-%s, TIME:%s:%s:%s] " % (now.day, now.month, now.year, now.hour, now.minute, now.second)
    status += "\nIP: {0}\nCurrent command: {1}".format([response['ip'][0]], str(response['current_command']))
    logging.info(status + '\n')