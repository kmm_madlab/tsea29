"""
The command line interface that controls the robot.
"""
import bluetooth_handler as b_handler
import json
import logging
import datetime
import keyboard_handler as k_h


def get_status(args):
    """
    Returns a JSON request for a status update.
    :param args: (List) List of arguments
    :return: (String) JSON request
    """
    assert isinstance(args, list)
    return json.dumps({"status": args})  # json.dumps() creates a json formatted string from a python object.


def walk(args):
    """
    Returns a JSON request for the robot to walk takes direction as firts element in list.
    :param args: (List) List of arguments
    :return: (String) JSON request
    """
    assert isinstance(args, list)
    directions = ["right", "forward", "backward", "left", "stop", "rotate_left", "rotate_right", "curve_right",
                  "curve_left"]
    if len(args) >= 1 and args[0] in directions:
        return json.dumps({"walk": args})
    else:
        raise ValueError("Invalid argument")


def steer(args):
    """
    Starts GUI for controling the robot with the arrow keys and space.
    :param args: (bluetooth socket) Bluetooth socket to send commands over.
    :return: (JSON String) JSON string with one last command.
    """
    return k_h.handle_user_input(args)


def auto(args):
    """
    Handles the input form the user and checks that the commands argument is given correcly.
    :param args: (List) List of arguments.
    :return: (JSON String) The new mode.
    """
    if not isinstance(args[0], str):
        return
    arg = args[0].lower()
    if arg in ['on', 'true', 'start']:
        return json.dumps({"auto": True})
    else:
        return json.dumps({"auto": False})

commands = {'status': get_status, 'walk': walk, 'steer': steer, 'auto': auto}


def format_status_dict(dictionary):
    """
    Formats a dictionary to key=value.
    :param dictionary: (dict) Dictionary to format.
    :return: (string) String containing the formated dictionary.
    """
    res = ""
    for ele in enumerate(dictionary):
        value = ele[0]
        name = ele[1]
        res += "{}={}, ".format(name, value)
    return res


def fix_lengt(word):
    """
    Takes a word and makes it to length 3 with spaces.
    :param word: (String)
    :return: (String)
    """
    while len(word) < 3:
        word += " "
    return word


def format_sensor_data(data):
    """
    Formats sensor data as a pretty picture of the robot with sensor values placed.
    :param data: (Dict) Dictionary with the sensor data.
    :return: (String) A string containing the pretty picture of the robot.
    """
    res = "       _        \n"
    res += " _____/ \_____ \n"
    res += "|             |\n"
    res += "|     " + fix_lengt(str(data['front'])) + "     |\n"
    res += "| " + fix_lengt(str(data['left_front'])) + " "*5 + fix_lengt(str(data['right_front'])) + " |\n"
    res += "|             |\n"
    res += "| " + fix_lengt(str(data['left_back'])) + " " * 5 + fix_lengt(str(data['right_back'])) + " |\n"
    res += "|     " + fix_lengt(str(data['back'])) + "     |\n"
    res += "|_____________|\n"
    return res


def handle_response(response):
    """
    Logs status and queued commands to file and saves it in a appropriate format.
    :param response: (JSON String)
    :return: (None)
    """
    now = datetime.datetime.now()
    response = json.loads(response)
    status = "\n[DATE: %s/%s-%s, TIME:%s:%s:%s] " % (now.day, now.month, now.year, now.hour, now.minute, now.second)
    status += "\nIP: {0}\nCurrent command: {1}\n".format(response['ip'][0], response['current_command'][0])
    status += "AUTO: {}\n".format(response['auto'])
    status += "SENSORS:\n"
    status += format_sensor_data(response['sensors'])
    logging.info(status + '\n')


def main():
    """
    Mainloop for the program that the user interacts with to control the robot. Takes commands in text form and sends
    them to the robot.
    :return: (None)
    """

    logging.basicConfig(filename="response.log", level=logging.DEBUG)
    exit_commands = ['q', 'exit', 'quit', 'end', 'stop']
    prompt = "Command: "

    print("Welcome to MADLABS amazing creation!")
    is_robot = input("Are you connecting to the robot? (y/n): ")
    is_robot = is_robot.lower()
    while len(is_robot) >= 1 and not is_robot[0] in ["y", "n"]:
        is_robot = input("Are you connecting to the robot? (y/n): ")
        is_robot = is_robot.lower()
    if is_robot[0] == "y":
        print("Connecting to robot")
        host = b_handler.robot_host
    else:
        print("Connecting to developing pi")
        host = b_handler.white_host
    b_socket = b_handler.connect((host, b_handler.port))
    handle_response(b_handler.send_message(get_status(list()), b_socket))  # First call for status (fixes steer bug).

    print("Connected")
    handle_response(b_handler.send_message(get_status(list()), b_socket))  # Get first status update

    while True:
        command = input(prompt)
        command = command.lower()

        if command in exit_commands:
            exit()

        # Take in a command until program is exited or valid command is given
        command = command.split(' ')
        while command[0] not in commands:
            print("%s not registered, the valid commands are:" % command[0])
            print(', '.join(commands))
            command = input(prompt)
            if command in exit_commands:
                exit()
            command = command.split(' ')

        # Execute command and receiving response from the robot.
        try:
            if command[0] == "steer":
                message = commands[command[0]](b_socket)
            else:
                message = commands[command[0]](command[1:])
        except ValueError as e:  # Catch bad arguments to commands
            print(e)
        response = b_handler.send_message(message, b_socket)
        handle_response(response)
        
    b_handler.disconnect(b_socket)


if __name__ == "__main__":
    main()
