import bluetooth

"""
Collection of function used for Bluetooth communication with the Raspberry Pi in the 'Kommunikatonsmodul'.
"""

# Server info
white_host = 'B8:27:EB:1D:71:40'  # Hampus Raspberry Pi's Mac address.
robot_host = 'B8:27:EB:C8:28:12'  # Robot Raspberry Pi's Mac address.
port = 3                          # Port of our choosing client and server must have the same.


def connect(server):
    """
    Connects to a bluetooth server and returns a bluetooth socket.
    :param server: (Tupel (String,Int)) Tupel containing the address and port of the server.
    :return: (BluetoothSocket) Socket for communication with the server.
    """
    bluetooth_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    bluetooth_socket.connect(server)
    return bluetooth_socket


def send_message(message, bluetooth_socket):
    """
    Sends a message to the server via the bluetooth socket and returns the response.
    :param message: (String) The message to send to the server.
    :param bluetooth_socket: (BluetoothSocket) Socket for communication with the server.
    :return: (List) Response from the server in the form of a list where each item are max 1024 bites.
    """
    try:
        response = ""
        bluetooth_socket.send(message)
        response_part = bluetooth_socket.recv(1024)

        # Response comes back as a bite string this function call gets it back to regular python string by decoding it.
        response_part = response_part.decode('utf-8')
        response += response_part

        while len(response_part) == 1008:  # For some reason the response will be a maximum of 1008 bites.
            response_part = bluetooth_socket.recv(1024)
            response_part = response_part.decode('utf-8')
            response += response_part

        return response

    except Exception as e:
        print(e)
        return None


def disconnect(bluetooth_socket):
    """
    Closes a connection with the server
    :param bluetooth_socket: Socket for communication with the server.
    :return: (None)
    """
    bluetooth_socket.close()
