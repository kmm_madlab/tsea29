"""
This file runs the GUI but first it sets the system path. The system path needs to be set before we import certain
packages.
"""
import sys
import config

if __name__ == "__main__":
    sys.path.append(config.INSTALL_PATH)
    
    import gui
    gui.run_gui()
