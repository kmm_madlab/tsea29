import pygame as pg


class Drawable:
    """
    Superclass Drawable which declars that all subclasses shall be able to draw it self.
    """
    
    def __init__(self, pos_x, pos_y):
        """
        Constructor for class.
        :param pos_x: (Int) Start position X.
        :param pos_y: (Int) Start position Y.
        """
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.rect = pg.Rect(pos_x, pos_y, 10, 10)
        
    def draw(self, screen):
        """
        Draw itself.
        :param screen: (Screen) Screen to draw on.
        :return: (None)
        """
        screen.fill(pg.Color(120, 120, 120, 255), self.rect)
        
    def move(self, new_x, new_y):
        """
        Move the drawable to new position.
        :param new_x: (Int) New position X.
        :param new_y: (Int) New position Y.
        :return:
        """
        self.rect = self.rect.move(new_x, new_y)


class Image(Drawable):
    """
    Sub class of Drawable and implements behavior for images.
    """
    
    def __init__(self, pos_x, pos_y, image_path):
        """
        Constructor of an Image.
        :param pos_x: (Int) Start position X.
        :param pos_y: (Int) Start position Y.
        :param image_path: (String) String containing the path to image.
        """
        super().__init__(pos_x, pos_y)
        self.image = pg.image.load(image_path)
        self.rect = self.image.get_rect()
        self.move(self.pos_x, self.pos_y)
    
    def draw(self, screen):
        """
        Specific draw function for images.
        :param screen: (Screen) Screen to draw on.
        :return: (None)
        """
        screen.blit(self.image, self.rect)
        

class TextBox(Drawable):
    """
    Class for drawable texts.
    """
    def __init__(self, column, row, text, font):
        """
        Constructor for TextBox objects.
        :param column: (Int) Column to draw text in.
        :param row: (Int) Row to draw text in.
        :param text: (Sting) Text.
        :param font: (Font) The font of the text.
        """
        self.line_height = 30
        self.line_width = 200
        self.border = 10
        super().__init__(self.margin_left(column), self.margin_top(row))
        self.font = font
        self.text = font.render(text, 1, (10, 10, 10))
        self.rect = self.text.get_rect()
        self.move(self.margin_left(column), self.margin_top(row))

    def draw(self, screen):
        """
        TextBox specific draw function.
        :param screen: (screen) Screen to daw on.
        :return: (None)
        """
        screen.blit(self.text, self.rect)

    def change_text(self, new_text):
        """
        Update the text.
        :param new_text: (String) New text.
        :return: (None)
        """
        self.text = self.font.render(new_text, 1, (10, 10, 10))
        self.rect = self.text.get_rect()
        self.move(self.pos_x, self.pos_y)

    def margin_left(self, column):
        """
        Adjust margin.
        :param column: (Int) The Column is wants to be drawn in.
        :return: (Int) Draw Position.
        """
        return self.border + self.line_width * column

    def margin_top(self, line):
        """
        Adjust margin.
        :param column: (Int) The Column is wants to be drawn in.
        :return: (Int) Draw Position.
        """
        return self.border + self.line_height * line