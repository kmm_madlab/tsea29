"""
Collection of GUI related function and status manipulation.
"""

from drawables import Image, TextBox
from communication import Connection
import pygame as pg
import os
import config

WIDTH = 1200
HEIGHT = 640
RESOLUTION = (WIDTH, HEIGHT)
IMAGE_DIR = os.path.join(config.INSTALL_PATH, "computer/GUI/images")


def check_status(status, new_status):
    """
    Checks if there is errors or missing values in the new status and only updates the status displayed with correct
    values.
    :param status: (Dict) The current status displayed in GUI.
    :param new_status: (Dict) The new status received from robot. (Get's modified)
    :return: (None)
    """
    if 'error' in new_status and new_status['error']:
        print("Error in new status")
        return
    for key in config.STATUS_KEYS:
        if key not in new_status:
            print("New status missing {}".format(key))
        else:
            status[key] = new_status[key]


def update_status_text(status, font):
    """
    Updates the status of the robot and adds it to the layer which will be drawn on the screen in the top left corner.
    :param status: (String) The sting of the status.
    :param font: (Font) The font used to draw text.
    :return: (List) The list of the new text in a list.
    """
    layer = list()
    
    # Exstracts from status.
    ip = status['ip'][0]
    curr_command = status['current_command'][0]
    sensors = status['sensors']
    
    # Adds all text boxes that shall be drawn on the screen.
    layer.append(TextBox(0, 0, "Front: {0}".format(sensors['front']), font))
    layer.append(TextBox(0, 1, "Left Front: {0}".format(sensors['left_front']), font))
    layer.append(TextBox(0, 2, "Left Back: {0}".format(sensors['left_back']), font))
    layer.append(TextBox(1, 0, "Back: {0}".format(sensors['back']), font))
    layer.append(TextBox(1, 1, "Right Front: {0}".format(sensors['right_front']), font))
    layer.append(TextBox(1, 2, "Right Back: {0}".format(sensors['right_back']), font))
    layer.append(TextBox(0, 3, "IP: {0}".format(ip), font))
    layer.append(TextBox(0, 4, "Current command: {0}".format(curr_command), font))
    
    return layer


def update_screen(layers, order, screen):
    """
    Updates the screen with content in layers, in the order spesified in order.
    :param layers: (Dict) The diffrent layer to draw on the screen.
    :param order: (Dict) The order to draw in.
    :param screen: (Screen) The screen to draw on.
    :return: (None)
    """
    
    for layer in order:
        for obj in layers[layer]:
            obj.draw(screen)
    
    pg.display.flip()


def handle_keyboard(event, connection):
    """
    Handles keyboard pygame events and transmit the information to the JSON file.
    :param event: (Event) A pygame event is for example a pressed key.
    :param connection: (Connection) A connection over bluetooth.
    :return: (None)
    """
    if event.key in [pg.K_w, pg.K_UP]:
        connection.steer('forward')
    elif event.key in [pg.K_a, pg.K_LEFT]:
        connection.steer('left')
    elif event.key in [pg.K_RIGHT, pg.K_d]:
        connection.steer('right')
    elif event.key in [pg.K_s, pg.K_DOWN]:
        connection.steer('backward')
    elif event.key in [pg.K_SPACE]:
        connection.steer('stop')

    elif event.key in [pg.K_2]:
        connection.steer('curve_left')
    elif event. key in [pg.K_3]:
        connection.steer('curve_right')

    elif event.key in [pg.K_z, pg.K_q]:
        connection.steer('rotate_left')
    elif event.key in [pg.K_x, pg.K_e]:
        connection.steer('rotate_right')

    elif event.key in [pg.K_t, pg.K_m]:
        connection.toggle_mode()
        

def handle_exit_events(event):
    """
    Handles the exit events.
    :param event: (Event) A pygame event is for example a pressed key.
    :return: (Boolean) Run the GUI.
    """
    running = True
    if event.type == pg.QUIT:
        running = False
    elif event.type == pg.K_ESCAPE:
        running = False
    return running


def display_active_command(active_command, layers):
    """
    Updated the active command by a look up dictionary in the active command list in the dictionary layers.
    :param active_command: (List) A list with a single element the current active command.
    :param layers: (Dict) Dict of all different layers that shall be drawn on the screen.
    :return: (None)
    """
    image_positions = {'stop': {'x': 600, 'y': 10, 'filename': "stop-red.png"},
                       'left': {'x': 625, 'y': 300, 'filename': "left-red.png"},
                       'right': {'x': 875, 'y': 300, 'filename': "right-red.png"},
                       'forward': {'x': 750, 'y': 200, 'filename': 'up-red.png'},
                       'backward': {'x': 750, 'y': 400, 'filename': "down-red.png"},
                       'rotate_left': {'x': 600, 'y': 450, 'filename': "rotating-left-red.png"},
                       'rotate_right': {'x': 900, 'y': 450, 'filename': "rotating-right-red.png"},
                       'curve_left': {'x': 600, 'y': 150, 'filename': 'left-curve-red.png'},
                       'curve_right': {'x': 900, 'y': 150, 'filename': 'right-curve-red.png'}}
    
    x = image_positions[active_command]['x']
    y = image_positions[active_command]['y']
    filename = image_positions[active_command]['filename']
    
    layers['active_command'] = [Image(x, y, os.path.join(IMAGE_DIR, filename))]


def display_mode(auto, layers):
    """
    Checks what mode we are in and draw the correct image to display the correct information.
    :param auto: (Boolean) Is auto mode on or off,(True or False).
    :param layers: (Dict) Dict of all different layers that shall be drawn on the screen.
    :return: (None)
    """
    if auto:
        layers['mode'] = [Image(0, 200, os.path.join(IMAGE_DIR, "ai.png"))]
    else:
        layers['mode'] = [Image(50, 200, os.path.join(IMAGE_DIR, "robot-new.png"))]


def gui_setup():
    """
    All images are loaded in to the layers dict, into the correct lists. Lists in dict is initialized.
    :return: (Dict) The dict containing the diffrent layer to draw on screen.
    """
    background_layer = list()
    text_layer = list()
    arrows = list()
    active_command = list()
    mode = list()
    
    layers = {'background': background_layer, 'text': text_layer, 'arrows': arrows, 'active_command': active_command,
              'mode': mode}
    
    background_layer.append(Image(0, 0, os.path.join(IMAGE_DIR, "background.png")))
    
    # Stop sign
    arrows.append(Image(600, 10, os.path.join(IMAGE_DIR, "stop.png")))
    
    # Black arrows
    arrows.append(Image(625, 300, os.path.join(IMAGE_DIR, "left.png")))
    arrows.append(Image(875, 300, os.path.join(IMAGE_DIR, "right.png")))
    arrows.append(Image(750, 200, os.path.join(IMAGE_DIR, "up.png")))
    arrows.append(Image(750, 400, os.path.join(IMAGE_DIR, "down.png")))
    
    #  Rotating
    arrows.append(Image(600, 450, os.path.join(IMAGE_DIR, "rotating-left.png")))
    arrows.append(Image(900, 450, os.path.join(IMAGE_DIR, "rotating-right.png")))

    # Smooth turns
    arrows.append(Image(900, 150, os.path.join(IMAGE_DIR, "right-curve.png")))
    arrows.append(Image(600, 150, os.path.join(IMAGE_DIR, "left-curve.png")))
    
    return layers


def control_robot(screen):
    """
    The main part of the GUI. Order of drawing is set. Always ends in setting the active command to stop.
    :param screen: (Screen) The Screen that should be drawn on.
    :return: (None)
    """
    connection = Connection(True)
    connection.configure()
    print("sent config")
    running = True
    font = pg.font.Font(None, 36)
    status = {'sensors': {'front': 0, 'left_front': 0, 'left_back': 0, 'right_front': 0, 'right_back': 0, 'back': 0},
              'ip': ['192.168.1.1'], 'current_command': 'stop', 'auto': False}
    
    order = ['background', 'text', 'arrows', 'mode', 'active_command']
    
    layers = gui_setup()
    
    update_counter = 0  # Used so that we don't ask for status every iteration of the loop.
    update_interval = 5  # The amount of iterations through the main loop before we ask for status again.
    
    # Retrieves status from robot
    new_status = connection.update_status()
    check_status(status, new_status)
    
    # Main loop for the GUI
    while running:
        
        # Update status
        if update_counter >= update_interval:
            new_status = connection.update_status()
            check_status(status, new_status)
            layers['text'] = update_status_text(status, font)
            update_counter = 0
        else:
            update_counter += 1
        
        # Handle events such as exit and keypress
        for event in pg.event.get():
            running = handle_exit_events(event)  # Shall we exit.
            if event.type == pg.KEYDOWN:
                handle_keyboard(event, connection)
        
        # Display mode and current command
        active_command = status['current_command'][0]
        display_active_command(active_command, layers)
        display_mode(status['auto'], layers)
        
        update_screen(layers, order, screen)
        pg.time.delay(10)
    connection.steer('stop')


def run_gui():
    """
    Sets the screen and starts the controll of robot.
    :return: (None)
    """
    pg.init()
    screen = pg.display.set_mode(RESOLUTION)
    control_robot(screen)
    pg.quit()
