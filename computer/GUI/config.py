INSTALL_PATH = "/home/hampus/kmm/"  # Change this to your own path!!!
STATUS_KEYS = ['sensors', 'ip', 'current_command', 'auto']

DEFAULT_CONFIG = {
    "ideal dist": 35,
    "proportional": 100,
    "break of dist": 135,
}


def verify_config(config):
    """
    Verifies that no config value is a predefined spi value or bigger than one byte.
    :param config: (Dict) The configuration for the control module
    :return: (None)
    """
    min_value = 15
    max_value = 255
    for key in config:
        assert min_value < config[key] < max_value
