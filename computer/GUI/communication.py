
"""
This file contains higher level functionality for communication between the robot and the computer.
"""

import computer.bluetooth_handler as bluetooth_handler
import bluetooth
import json
import config
import os


class Connection:
    """
    Declare proprieties of a bluetooth connection between a computer and a robot (raspberry pi).
    """
    def __init__(self, connect_to_robot):
        """
        Constructor of a Bluetooth connection.
        :param connect_to_robot: (Boolean) Shall we connect to robot or development pi.
        """
        self.status = dict()
        if connect_to_robot:
            self.host = bluetooth_handler.robot_host
        else:
            self.host = bluetooth_handler.white_host
        self.port = bluetooth_handler.port
        self.socket = bluetooth_handler.connect((self.host, self.port))
        cm_config_file = open(os.path.join(config.INSTALL_PATH, "computer/cm_config.json"))
        try:
            self.config_data = json.load(cm_config_file)
        except Exception as e:
            print(e)
            self.config_data = config.DEFAULT_CONFIG
        
    def send(self, message):
        """
        Sends a message to the robot over bluetooth and waits for the response. Also handles errors related to bad
        response.
        :param message: (String) JSON formatted python string.
        :return: (Dict) Dict containing the response from the robot.
        """
        response = None
        try:
            response = bluetooth_handler.send_message(message, self.socket)
        except bluetooth.btcommon.BluetoothError as e:
            print(e)
        if response:
            response = json.loads(response)
        else:
            response = {"error": True}
        return response
    
    def reconnect(self):
        """
        Is called if connection is lost, closes the used socket. Tries to reopen a new connection.
        :return: (None)
        """
        self.socket.close()
        bluetooth_handler.connect((self.host, self.port))

    def update_status(self):
        """
        Updates it's own status and sends it to the raspberry pi and also returns it.
        :return: (String) The new status.
        """
        self.status = self.send(json.dumps({'status': []}))
        return self.status

    def steer(self, direction):
        """
        Sends the information about the steering command to the raspberry pi. Also is responsible for turning of auto
        mode if user hit any steering key during auto mode is on.
        :param direction: (String) The direction to walk in represented by a string.
        :return: (JSON String) JSON request.
        """
        if self.status['auto']:
            self.send(json.dumps({'auto': False}))
        return self.send(json.dumps({'walk': [direction]}))

    def toggle_mode(self):
        """
        Toggle on and of the auto mode.
        :return: (JSON String) Auto mode on or off..
        """
        if self.status['auto']:
            return self.send(json.dumps({'auto': False}))
        else:
            return self.send(json.dumps({'auto': True}))
    
    def configure(self):
        """
        Sends the current configuration to the robot
        :return: (JSON String) The response from the robot.
        """
        args = [self.config_data['break of dist'],
                self.config_data['proportional'],
                self.config_data['ideal dist'],
                self.config_data['dist scalar'],
                self.config_data['angle scalar']]
        return self.send(json.dumps({'config': args}))
