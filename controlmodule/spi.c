/***********************************************************************/
/* SPI related initialization. Executes only once. */
/***********************************************************************/
enum spiState {IDLE, SEND, RECEIVE};
enum spiState currentSpiState = IDLE;
/*	
*	Arrays containing the data to send with the next SPI-message and the data received in the last. 
*	Maximum size of message is 255 Bytes. 
*/
uint8_t message[255];
	
uint8_t spiSendBuffer[255] = {2,3};
uint8_t spiSendIndexer = 0;
uint8_t spiReceiveBuffer[255];
uint8_t spiReceiveIndexer = 0;

/* Constants that are commonly used in the SPI-functions */
#define ASCII_NULL 0x00	// ASCII value for NULL
#define STX 0x02		// ASCII start of text
#define ETX 0x03		// ASCII end of text
#define EOT 0x04		// ASCII end of transmission
#define ACK 0x06		// ASCII acknowledge
#define NAK 0x15		// ASCII negative acknowledge

/* When spiInProgress is set to true no operations are allowed on the spiSend- and spiReceiveBuffers as this could corrupt the data */
bool spiInProgress = false;


void spi_init(void)
{
	// Initialize SPI Slave Device 
	
	/*	
	*	BIT7: SPI Interrupt Enable
	*	BIT6: SPI Enable
	*	BIT5: Data order, MSB first
	*	BIT4: Set as slave
	*	BIT3: Clock polarity
	*	BIT2: Clock phase
	*	BIT1-0: SPI clock divider, CPU/64 
	*/
	SPCR = 0b11000010;
}

/***********************************************************************/
/* SPI related functions. Executes more than once. */
/***********************************************************************/


void spi_place_to_buffer(uint8_t values[], uint8_t length)
{
	/* 
	*	Used to place an array of values in the spiSendBuffer 
	*	:Param values:	(uint8_t) list of the values to be sent over SPI.
	*	:Param length:	(uint8_t) length of the message to be sent.	
	*/
	
	// Set first element to STX.
	spiSendBuffer[0] = STX;
	uint8_t bufferIndex = 0;
	
	// place message in buffer.
	while(bufferIndex < length)
	{
		spiSendBuffer[bufferIndex+1] = values[bufferIndex];
		bufferIndex++;
	}
	
	// set last element to ETX.
	spiSendBuffer[bufferIndex+1] = ETX;
}


uint8_t *spi_read_from_buffer(int *length) 
{
	/*
	*	Reads message from the SPI buffer.
	*	:return: pointer to the first element in the message-list
	*
	*/
	uint8_t bufferIndex = 0;
	uint8_t messageIndex = 0;
	bool STXFound = false;
	memset(message, 0, sizeof(message));
	
	// Continue to read the message byte by byte until we receive ETX or we reach the end of the lsit.
	while(spiReceiveBuffer[bufferIndex] != ETX && bufferIndex < 255){
		
		// Are we in the beginning of the message? Then do nothing.
		if(spiReceiveBuffer[bufferIndex] == STX){
			STXFound = true;
		
		// If we are "in" the message keep reading bytes.
		}else if(STXFound == true){
			message[messageIndex] = spiReceiveBuffer[bufferIndex];
			messageIndex++;
		}
		
		bufferIndex++;
	}
	
	// Was there a message to be read? Then return the read message.
	if(STXFound == true){
		*length = messageIndex;
		return message;
	}
	
	// If no message was found return null.
	*length = 0;
	return 0;
}


void spi_acknowledge_command(uint8_t mode) 
{
	/*
	*	Used to acknowledge the com-module that a mode (ex. MANUAL) has been received.
	*	:Param mode: (uint8_t) the mode that has been received.
	*/
	uint8_t message[] = {ACK , mode};
	spi_place_to_buffer(message, 2);
}


ISR(SPI_STC_vect)
{	
	/*
	*	 Interrupt vector for the SPI, will trigger after a SPI-transmission is completed (1 Byte).
	*/
	
	uint8_t byteToSend = spiSendBuffer[spiSendIndexer];
	
	switch (currentSpiState)
	{
		case IDLE :

			if(SPDR == ASCII_NULL) {
				//send start of text
				SPDR = STX;
				spiSendIndexer++;
				currentSpiState = SEND;
				spiInProgress = true;
			} else {
				SPDR = EOT;
			}
			break;
		 
		case SEND :
			if(byteToSend != ETX) {
				//keep saving data
				spiReceiveBuffer[spiReceiveIndexer] = SPDR;
				spiReceiveIndexer++;
				//keep sending data
				spiSendIndexer++;
				SPDR = byteToSend;
			} else if (spiReceiveIndexer >= 255) { //If ETX never received
				SPDR = EOT;
				spiReceiveBuffer[spiReceiveIndexer] = ETX;
				currentSpiState = IDLE;
			} else {
				//keep saving data
				spiReceiveBuffer[spiReceiveIndexer] = SPDR;
				spiReceiveIndexer++;
				//send ETX
				SPDR = byteToSend;
				currentSpiState = RECEIVE;				
			}
			break;
		
		case RECEIVE :
			if (SPDR != ETX) {
				//keep saving data
				spiReceiveBuffer[spiReceiveIndexer] = SPDR;
				spiReceiveIndexer++;				
				//keep sending ETX
				SPDR = ETX;
			} else if (spiReceiveIndexer >= 255) { //If ETX never received
				SPDR = EOT;
				spiReceiveBuffer[spiReceiveIndexer] = ETX;
				currentSpiState = IDLE;
			} else {
				//End of message, reset.
				spiReceiveBuffer[spiReceiveIndexer] = ETX;
				SPDR = ASCII_NULL;
				spiSendIndexer = 0;
				spiReceiveIndexer = 0;
				currentSpiState = IDLE;
				spiInProgress = false;
			}
			break;
	}
}
