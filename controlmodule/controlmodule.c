/*
 * controlmodule.c
 *
 * Updated: 2016-12-13
 * Author: MADLAB
 */ 


#define F_CPU 16000000UL // Clock frequency = 16 MHz. Must come before "#include <util/delay.h>"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>
#include <inttypes.h>
#include <math.h>
#include "tables.h"
#include "uart.c"
#include "spi.c"

/* Movement related constants */
#define POSITION_DELAY 20 // Time in ms to wait after assigning robot new position
#define TOPDOWN 1 // Order in which to read movement table
#define BOTTOMUP 0


/*
*	constants for mode selection
*/
const uint8_t AUTO = 49;
const uint8_t MANUAL = 50;
const uint8_t CONFIG = 51;

/*
*	config values
*/
uint8_t breakof_distance = 149;

/*
*	Constants for command selection
*/
#define FRWRD 49
#define RTRIGHT 50
#define RTLEFT 51
#define RIGHT 52
#define LEFT 53
#define BKWRD 54
#define STP 55
#define TURNRIGHT 56
#define TURNLEFT 57

/*
* Enum for movement states
*/
enum State {
	CORRIDOR,
	RIGHT_TURN,
	LEFT_TURN,
	RIGHT_ROTATE,
	LEFT_ROTATE,
	EXITING_INTERSECTION
};

/*
* The robot's current movement state
*/
enum State current_state = CORRIDOR;
bool regulate_right = true;

/*
* Used in AI and regulator.
* KP_var is an adjustment variable for the proportional regulator part.
* KD_var is an adjustment variable for the derivative regulator part.
* The constant ideal_distance is the desired distance from wall to the robot's center.
*/
const double distance_between_sensors = 13.8;
double ideal_distance = 32;
double KP_var = 2; // From com.module '1' would be represented as '100',
double dist_scalar = 1.25;
double angle_scalar = 65;

/***********************************************************************/
/* General initialization and functions. */
/***********************************************************************/


void define_io_ports (void) {
	/*	
	*	Define all "don't care" as '0' (input), this activates the internal pull-up.
	*	Define all pins on port A as OUTPUT.
	*/
	DDRA = 0b11111111;
	
	/*	B7 = INPUT, SPI master clock.
	*	B6 = OUTPUT, MISO.
	*	B5 = INPUT, MOSI.
	*	B4 = INPUT, SS.
	*	B3 = INPUT, Don't care.
	*	B2 = INPUT, Don't care.
	*	B1 = INPUT, Don't care.
	*	B0 = INPUT, Don't care.
	*/
	DDRB = 0b01000000;
	
	//	DDRC is only used by JTAG, so we leave it untouched.
	
	/*	D7 = INPUT, Don't care.
	*	D6 = INPUT, Don't care.
	*	D5 = INPUT, Don't care.
	*	D4 = INPUT, Don't care.
	*	D3 = INPUT, Don't care.
	*	D2 = OUTPUT, USART0 TX/RX-toggle.
	*	D1 = OUTPUT, USART0 TX-transmitter.
	*	D0 = INPUT, USART0 RX-receiver.
	*/
	DDRD = 0b00000110;
	
	// Turn off all LEDs (A0-A7) by clearing all pins on port A.
	PORTA = 0b00000000;
}


void indicate_ready (void) {
	/*
	*	Indicate on the leds that the module is ready to go.
	*/
	
	PORTA |= (1<<PORTA0); _delay_ms(125);
	PORTA |= (1<<PORTA1); _delay_ms(125);
	PORTA |= (1<<PORTA2); _delay_ms(125);
	PORTA |= (1<<PORTA3); _delay_ms(125);
	PORTA |= (1<<PORTA4); _delay_ms(125);
	PORTA |= (1<<PORTA5); _delay_ms(125);
	PORTA |= (1<<PORTA6); _delay_ms(125);
	PORTA |= (1<<PORTA7); _delay_ms(125);
	PORTA = 0b00000001;	// Leaving PORT0 on to indicate "OK!"
}


void update_config (uint8_t config[]) {
	/*
	*	Update the config values received from com-module.
	*	:Param: (uint8_t) config list of config parameters.
	*/
	breakof_distance = (double) config[2];
	double new_KP_var = (double) config[3];
	KP_var = new_KP_var/100;
	ideal_distance = config[4];
	double new_dist_scalar = (double) config[5];
	dist_scalar = new_dist_scalar/100;
	angle_scalar = config[6];
}

/***********************************************************************/
/* Movement Functions. NOTE: Must be declared below movement tables!   */
/***********************************************************************/


void read_table(const uint8_t table[][6][6], bool topDown, int nPos) {
	/*
	*	Reads a movement table, top down or bottom up. All servo settings in the
	*	table are applied to the robot's servos.
	*
	*	Params:
	*   - const uint8_t table[][6][6]: The table to read.
	*   - bool topDown: Set to true if the table is to be read top down.
	*   - int nPos: The number of entries in the table.
	*/
	
	int sInd = 0;		// Start index
	int eInd = nPos-1;  // End index
	int dif = 1;	    // Each loop, how many steps down do we take? 1 if topDown, -1 if !topDown.
	
	if (!topDown){
		sInd = eInd;
		eInd = 0;
		dif = -1;
	}
	
	for (int position = sInd; position != eInd; position += dif)
	{
		for (int leg = 0 ;leg < 6; leg++)
		{
			for (int servo = 0; servo < 3; servo++)
			{
				parameters[0] = 0x1E;
				parameters[1] = pgm_read_byte(&(table[position][leg][2 * servo]));
				parameters[2] = pgm_read_byte(&(table[position][leg][2 * servo + 1]));
				parameters[3] = 0x00;
				parameters[4] = 0x02;
				transmit_instruction(servoID(leg, servo), INSTR_REG_WRITE, 5);
			}
		}
		transmit_instruction(BROADCASTING_ID, INSTR_ACTION, 0);
		_delay_ms(POSITION_DELAY);
	}
}


/*
*	Routines where all servos are iterate thought and info from tables in tables.h is written to the appropriate place.
*/
void stand_up(void) {
	read_table(table_stand, TOPDOWN, 46);
	_delay_ms(500);
}

void lie_down(void) {
	read_table(table_stand, BOTTOMUP, 46);
	_delay_ms(3000);
}

void walk_forwards(void) {
	read_table(table_forward, TOPDOWN, 41);
}

void walk_backwards(void) {
	read_table(table_forward, BOTTOMUP, 41);
}

void rotate_right(void) {
	read_table(table_rotate, TOPDOWN, 41);
}

void rotate_left(void) {
	read_table(table_rotate, BOTTOMUP, 41);
}

void turn_right_easy1(void) {
	read_table(table_right_easy1, TOPDOWN, 41);
}

void turn_right_easy2(void) {
	read_table(table_right_easy2, TOPDOWN, 41);
}

void turn_right_medium1(void) {
	read_table(table_right_medium1, TOPDOWN, 41);
}

void turn_right_medium2(void) {
	read_table(table_right_medium2, TOPDOWN, 41);
}

void turn_right_hard(void) {
	read_table(table_right_hard, TOPDOWN, 41);
}

void back_up_right_medium1(void) {
	read_table(table_right_medium1, BOTTOMUP, 41);
}

void turn_left_easy1(void) {
	read_table(table_left_easy1, TOPDOWN, 41);
}

void turn_left_easy2(void) {
	read_table(table_left_easy2, TOPDOWN, 41);
}

void turn_left_medium1(void) {
	read_table(table_left_medium1, TOPDOWN, 41);
}

void turn_left_medium2(void) {
	read_table(table_left_medium2, TOPDOWN, 41);
}

void turn_left_hard(void) {
	read_table(table_left_hard, TOPDOWN, 41);
}

void back_up_left_medium1(void) {
	read_table(table_left_medium1, BOTTOMUP, 41);
}

void walk_right(void) {
	read_table(table_crab, TOPDOWN, 41);
}

void walk_left(void) {
	read_table(table_crab, BOTTOMUP, 41);
}

void sneak(void) {
	read_table(table_sneak, TOPDOWN, 41);
}

void stop(void) {
	read_table(table_stop, TOPDOWN, 2);
}


void manual_mode(uint8_t command){
	/*
	*	This function decides what to do when a command is send from the com-module
	*
	*	:Param: (uint8_t) the command to be executed.
	*/
	
	switch (command) {
		case FRWRD:
			walk_forwards();
		break;
		case RTRIGHT:
			rotate_right();
		break;
		case RTLEFT:
			rotate_left();
		break;
		case RIGHT:
			walk_right();
		break;
		case LEFT:
			walk_left();
		break;
		case BKWRD:
			walk_backwards();
		break;
		case STP:
			stop();
		break;
		case TURNRIGHT:
			turn_right_medium2();
		break;
		case TURNLEFT:
			turn_left_medium2();
		break;
		default:
			stop();
		break;
	}
}

double calculate_regulation(uint8_t* new_sensor_values, bool right) {

	/*
	* Origo for choosing tables and regulating.
	*/
	double baseline = 128;
	double front;
	double back;
	double angle_to_wall;
	double distance_to_wall;
	double error;

	if (right) {
		back = new_sensor_values[2];
		front = new_sensor_values[3];

	} else {
		back = new_sensor_values[4];
		front = new_sensor_values[5];
	}

	/*
    * Calculates angle between robot and wall to the right with the help of a right-angled triangle and arctan.
    * The difference right_back-right_front gives us the opposing cathetus.
    * The constant distance_between_sensors gives us the nearby cathetus.
    * Function atan returns a value in radians between -PI/2 to +PI/2.
    */
    angle_to_wall = atan((back - front) / distance_between_sensors); // Res 8.2 deg.

    if (!right) {
        angle_to_wall *= -1;
    }
	
	/* 
	* Calculates distance between centerpoint of robot and wall to the right.
	* The quota (right_back+right_front)/2 is the uncorrected mean distance.
	* Function cos corrects the mean distance relative to angle_to_wall.
	*/
	distance_to_wall = ((back + front) / 2) * cos(angle_to_wall);
	
	/*
	* Error is the difference between ideal_distance and distance_to_wall.
	*/
	error = (ideal_distance - distance_to_wall);

	if (!right) {
	    error *= -1;
	}

	
	/*
	* Value between -70/-100 and 70/100 for our use-cases. We might want to change this afterwards.
	*/
	double affector = -KP_var*(dist_scalar * error + (angle_scalar * angle_to_wall));

	
	return (baseline+affector);
}

void regulator(double regulateValue) {
	// Keep walking straight forward.
	if (118 < regulateValue && regulateValue <= 138) {
		walk_forwards();
	}
	
	//Adjust to the right (Approx. span of 20).
	else if (138 < regulateValue && regulateValue <= 153) {
		turn_right_easy1();
	}
	else if (153 < regulateValue && regulateValue <= 168) {
		turn_right_easy2();
	}
	else if (168 < regulateValue && regulateValue <= 183) {
		turn_right_medium1();
	}
	else if (183 < regulateValue && regulateValue <= 198) {
		turn_right_medium2();
	}
	else if (198 < regulateValue) {
		turn_right_hard();
	}
	
	//Adjust to the left (Approx. span of 20).
	else if (103 < regulateValue && regulateValue <= 118) {
		turn_left_easy1();
	}
	else if (88 < regulateValue && regulateValue <= 103) {
		turn_left_easy2();
	}
	else if (73 < regulateValue && regulateValue <= 88) {
		turn_left_medium1();
	}
	else if (58 < regulateValue && regulateValue <= 73) {
		turn_left_medium2();
	}
	else if (regulateValue <= 58) {
		turn_left_hard();
	}
}

bool front_pocket = false;

void auto_mode(uint8_t* message) {
	uint16_t front_sensor = message[1];
	uint16_t right_rear_sensor = message[2];
	uint16_t right_front_sensor = message[3];
	uint16_t left_rear_sensor = message[4];
	uint16_t left_front_sensor = message[5];
	uint16_t rear_sensor = message[6];
	uint16_t FRL = right_front_sensor + left_front_sensor;
	uint16_t diag = 113; // roughly sqrt(80^2 + 80^2)

	// STATE: CORRIDOR
	if (current_state == CORRIDOR) {
		
		// Do the sensor values indicate a corridor?
		if ((rear_sensor + front_sensor) <= diag || FRL <= diag) {
			regulator(calculate_regulation(message, regulate_right));
		}
		
		// Right front sensor unhindered?
		else if (right_front_sensor >= breakof_distance){
			front_pocket = front_sensor >= 120;
			current_state = RIGHT_TURN;
		}
		
		// Left front sensor unhindered?
		else if (left_front_sensor >= breakof_distance) {
			front_pocket = front_sensor >= 120;
			current_state = LEFT_TURN;
		}
		
		else {
			walk_forwards();
		}
	}

	// STATE: RIGHT_TURN
	else if (current_state == RIGHT_TURN) {
		if (!front_pocket && front_sensor <= 41){
			current_state = RIGHT_ROTATE;
			regulate_right = false;

		}
		else if (front_pocket && front_sensor <= 126){ // 120
			current_state = RIGHT_ROTATE;
		} 
		else  {
			walk_forwards();
		}
	}
	
	// STATE: LEFT_TURN
	else if (current_state == LEFT_TURN) {
		if (!front_pocket && front_sensor <= 41){
			current_state = LEFT_ROTATE;
			regulate_right = true;
		}
		else if (front_pocket && front_sensor <= 126){ // 120
			current_state = LEFT_ROTATE;
		}
		else  {
			walk_forwards();
		}
	}
		
	// STATE: RIGHT_ROTATE
	else if (current_state == RIGHT_ROTATE) {
		if (front_sensor >= 149) {
			current_state = EXITING_INTERSECTION;
		} else {
			rotate_right();
		}
	}
	
	// STATE: LEFT_ROTATE
	else if (current_state == LEFT_ROTATE) {
		if (front_sensor >= 149) {
			current_state = EXITING_INTERSECTION;
			} else {
			rotate_left();
		}
	}
	
	//STATE: EXITING_INTERSECTION
	else if (current_state == EXITING_INTERSECTION) {
		if (right_rear_sensor + left_rear_sensor <= diag) {
			current_state = CORRIDOR;
		} else {
			walk_forwards();
		}
	}
}

/***********************************************************************/
/* Main Loop */
/***********************************************************************/

/*
*	Disables interrupts while init routines are called and then turn interrupts back on. Then starts an infinit loop where walking commands are
*	placed. The loop also checks for new config messages from the com. module
*/
int main(void) {
	
	const uint8_t MODEINDEX = 0;
	const uint8_t COMMANDINDEX = 1;
	uint8_t command;
	uint8_t *spiMessage = '\0';
	int spiMessageLength;
	
	cli(); // Disable global interrupts to provide a safe initialization.
	define_io_ports();
	uart_init(DEFAULT_BAUD_RATE);
	spi_init();
	stand_up(); // Delayed by 500 ms, allows enough time to actually stand.
	stop();
	sei(); // Enable global interrupts.
	indicate_ready();
	
	/*
	*	The actual main loop where we handles three different modes: CONFIG, MANUAL and AUTO.
	*	In this version we only use the two first modes.
	*/
	
	while (1) {
		// If the spi process has been completed start a new one.
		if(spiInProgress == false){
			cli();
			spiMessage = spi_read_from_buffer(&spiMessageLength);
			spi_acknowledge_command(spiMessage[MODEINDEX]);	
			sei();
		}
		//PORTA = angle_scalar;
		// Choose what to do depending on the mode we are in.
		if(spiMessage[MODEINDEX] == MANUAL){
			// Control the robot manually.
			if (spiMessageLength == 2) {
			    current_state = CORRIDOR;
                regulate_right = true;
				command = spiMessage[COMMANDINDEX];
				manual_mode(command);
			}
		} else if (spiMessage[MODEINDEX] == AUTO) {
			if (spiMessageLength == 7) {
			auto_mode(spiMessage);
			}
		} else if (spiMessage[MODEINDEX] == CONFIG) {
			// Update the config variables.
			if(spiMessageLength == 7) {
			    current_state = CORRIDOR;
			    regulate_right = true;
				update_config(spiMessage);
			}
		} else {
				PORTA = 0b11111111;
		}
	}
}