/***********************************************************************/
/* USART related initialization. Executes only once. */
/***********************************************************************/
//#define _BV(n) (1 << n) // Produces a byte with only byte n set.
#define ENABLE_BIT_DEFINITIONS

/* Set a bit in an 8 bit register */
#define sbi(REG,BIT) REG |= (1 << BIT)
/* Clear a bit in an 8 bit register */
#define cbi(REG,BIT) REG &= ~(1 << BIT)

/* AX12A Control Table EEPROM Area addresses */
#define P_MODEL_NUMBER_L 0
#define P_MODEL_NUMBER_H 1
#define P_VERSION 2
#define P_ID 3
#define P_BAUD_RATE 4
#define P_RETURN_DELAY_TIME 5
#define P_CW_ANGLE_LIMIT_L 6
#define P_CW_ANGLE_LIMIT_H 7
#define P_CCW_ANGLE_LIMIT_L 8
#define P_CCW_ANGLE_LIMIT_H 9
#define P_SYSTEM_DATA2 10
#define P_LIMIT_TEMPERATURE 11
#define P_DOWN_LIMIT_VOLTAGE 12
#define P_UP_LIMIT_VOLTAGE 13
#define P_MAX_TORQUE_L 14
#define P_MAX_TORQUE_H 15
#define P_RETURN_LEVEL 16
#define P_ALARM_LED 17
#define P_ALARM_SHUTDOWN 18
#define P_OPERATING_MODE 19
#define P_DOWN_CALIBRATION_L 20
#define P_DOWN_CALIBRATION_H 21
#define P_UP_CALIBRATION_L 22
#define P_UP_CALIBRATION_H 23

/* AX12A Control Table RAM Area addresses */
#define P_TORQUE_ENABLE (24)
#define P_LED (25)
#define P_CW_COMPLIANCE_MARGIN (26)
#define P_CCW_COMPLIANCE_MARGIN (27)
#define P_CW_COMPLIANCE_SLOPE (28)
#define P_CCW_COMPLIANCE_SLOPE (29)
#define P_GOAL_POSITION_L (30)
#define P_GOAL_POSITION_H (31)
#define P_GOAL_SPEED_L (32)
#define P_GOAL_SPEED_H (33)
#define P_TORQUE_LIMIT_L (34)
#define P_TORQUE_LIMIT_H (35)
#define P_PRESENT_POSITION_L (36)
#define P_PRESENT_POSITION_H (37)
#define P_PRESENT_SPEED_L (38)
#define P_PRESENT_SPEED_H (39)
#define P_PRESENT_LOAD_L (40)
#define P_PRESENT_LOAD_H (41)
#define P_PRESENT_VOLTAGE (42)
#define P_PRESENT_TEMPERATURE (43)
#define P_REGISTERED_INSTRUCTION (44)
#define P_PAUSE_TIME (45)
#define P_MOVING (46)
#define P_LOCK (47)
#define P_PUNCH_L (48)
#define P_PUNCH_H (49)

/* AX12A instruction set */
#define INSTR_PING 0x01
#define INSTR_READ_DATA 0x02
#define INSTR_WRITE_DATA 0x03
#define INSTR_REG_WRITE 0x04
#define INSTR_ACTION 0x05
#define INSTR_RESET 0x06
#define INSTR_SYNC_WRITE 0x83
#define BROADCASTING_ID 0xfe

/* Baud rate constant, to be put in UBRR0. See table 17-1 in the 1284P data sheet for equations for calculating baud rate */
#define DEFAULT_BAUD_RATE 0 // 1Mbps at 16MHz
#define RX_TIMEOUT_COUNT (30000L) // Time to wait for status packets before timing out
#define MININUM_STATUS_PACKET_SIZE 6 // Status packet size with 0 parameters

/* USART 0 */
#define ENABLE_TXD sbi(PORTD, PORTD2); // Control signal for the AND-gates in the line driver 74LS241
#define TXD_READY bit_is_set(UCSR0A, UDRE0) // p179. The UDREn flag is 1 when the data register is empty (previous transmission may not be complete yet)
#define TXD_DATA (UDR0) // Data register containing the byte of data that will be shifted onto the UART.
#define ENABLE_RXD cbi(PORTD, PORTD2); // Control signal for the AND-gates in the line driver 74LS241
#define RXD_READY bit_is_set(UCSR0A, RXC0) // p183. The RXCn flag is 1 when there is unread data present in the receive buffer.
#define RXD_DATA (UDR0)
#define TXD_FINISHED bit_is_set(UCSR0A, TXC0) // p176. The TXCn flag is 1 when the data register is empty and transmission is finished.
#define SET_TXD_FINISH sbi(UCSR0A, TXC0)
#define CLEAR_TXD_FINISH cbi(UCSR0A, TXC0)

/* Global variables for transmitting and receiving packets */
uint8_t parameters[128];
uint8_t RXbuffer[128];
uint8_t TXbuffer[128];

// Set Baud rate, enable RX/TXD and set frame size. (More in datasheet, section 17.6)
void uart_init(uint8_t bBaudrate)
{
	UBRR0H = 0;
	UBRR0L = bBaudrate;
	UCSR0B = (1<<RXEN0) | (1<<TXEN0); // Enable RXD0 and TXD0 on pin PD0 and PD1
	UCSR0C = (1<<UCSZ00) | (1<<UCSZ01); // 8 Bit USART frame size and 1 stop bit
	cbi(UCSR0A, U2X0); // Don't use double speed (affects baud rate generator, see table 17.1 in the 1284 data sheet)
	ENABLE_RXD;
	SET_TXD_FINISH;
}

/***********************************************************************/
/* USART related functions. Executes more than once. */
/***********************************************************************/

/* uart_putByte(): transmit one byte of data over USART 0. */
void uart_putByte(uint8_t bData)
{
	while(!TXD_READY) // Wait until data register is empty
	;
	TXD_DATA = bData;
}

/* uart_getByte(): receive one byte of data over USART 0. */
uint8_t uart_getByte(void)
{
	while(!RXD_READY) // Wait until there's unread data in the data register
	;
	return(RXD_DATA);
}

/* transmit_instruction(): Transmits one instructon packet over USART 0. Puts sent data in TXbuffer, length of data is returned. */
uint8_t transmit_instruction(uint8_t bID, uint8_t bInstruction, uint8_t bParameterLength) 
{
	uint8_t bCount,bCheckSum,bPacketLength;
	
	// 0xFF 0xFF 0x01 0x02
	TXbuffer[0] = 0xff;
	TXbuffer[1] = 0xff;
	TXbuffer[2] = bID;
	TXbuffer[3] = bParameterLength + 2;
	TXbuffer[4] = bInstruction;
	
	for(bCount = 0; bCount < bParameterLength; bCount++) 
	{
		TXbuffer[bCount + 5] = parameters[bCount];
	}
	bCheckSum = 0;
	bPacketLength = bParameterLength + 4 + 2; // TODO
	
	for(bCount = 2; bCount < bPacketLength - 1; bCount++) // except 0xff,checksum
	{ 
		bCheckSum += TXbuffer[bCount];
	}
	TXbuffer[bCount] = ~bCheckSum; // Writing Checksum with Bit Inversion
	cli(); // Disable interrupts
	ENABLE_TXD;
	
	for(bCount = 0; bCount < bPacketLength; bCount++) 
	{
		SET_TXD_FINISH; // p176, sec17.6. Clear TXC before writing to UDR.
		uart_putByte(TXbuffer[bCount]); // Transmit one byte over USART
	}
	while(!TXD_FINISHED) // Wait until TXD Shift register empty
	;
	ENABLE_RXD;
	sei(); // Enable interrupts
	
	return(bPacketLength);
}

/* receive_status(): Receives one status packet over USART 0. Data is put in RXbuffer, length of data is returned. */
uint8_t receive_status(uint8_t bRxPacketLength) 
{
	unsigned long ulCounter;
	uint8_t bCount, bLength, bChecksum;
	uint8_t bTimeout = 0;
	
	for(bCount = 0; bCount < bRxPacketLength; bCount++)	
	{
		ulCounter = 0;
		while(!RXD_READY) 
		{
			if(ulCounter++ > RX_TIMEOUT_COUNT) 
			{
				bTimeout = 1;
				break;
			}
		}
		if(bTimeout) break;
		RXbuffer[bCount] = uart_getByte();
	}
	bLength = bCount;
	bChecksum = 0;

	if(TXbuffer[2] == BROADCASTING_ID) 
	{
		return 0; // Ignore status packages when broadcasting
	}
	if(bTimeout) 
	{
		return 0; // Timed out
	}
	if(bLength > 3)	
	{
		if(RXbuffer[0] != 0xff || RXbuffer[1] != 0xff )	
		{
			return 0; // All valid status packages start with 0xFF 0xFF
		}
		if(RXbuffer[2] != TXbuffer[2]) 
		{
			return 0; // All status packages must be from whatever servo we previously transmitted to
		}
		if(RXbuffer[3] != bLength - 4) 
		{
			// "length" in the status package is "number of parameters + 2".
			// This must equal "total length of package - 4 (2 for 0xff, 1 for the "length" byte, 1 for the checksum)"
			return 0;
		}
		// calculate checksum as NOT(id+length+error+parameter1+...parameter n)
		// note that AX12 manual incorrectly says that "instruction" is included instead of "error"
		for(bCount = 2; bCount < bLength - 1; bCount++) 
		{
			bChecksum += RXbuffer[bCount];
		}
		if(~bChecksum != RXbuffer[bLength - 1]) 
		{
			return 0; // Incorrect checksum
		}
	}
	return bLength;
}

/*
*	Returns the correct ID to the servos.
*	Input: the specific leg {0,...,5} and the specific joint {0,...,2}
*	Return: corresponding servo-ID
*
*	Position of legs: 0 = front left, 1 = front right, 2 = rear left, 3 = rear right, 4 = middle left, 5 = middle right
*	Position of joint: 0 = hip, 1 = knee, 2 = ankle
*/
uint8_t servoID(int leg, int joint)
{

	uint8_t ID_table[6][3] = 
	{
		//	Front legs 
		{0x01, 0x03, 0x05},	//left	
		{0x02, 0x04, 0x06}, //right
		//	Rear legs
		{0x07, 0x09, 0x0B}, //left
		{0x08, 0x0A, 0x0C}, //right
		// Middle legs
		{0x0D, 0x0F, 0x11}, //left
		{0x0E, 0x10, 0x12}  //right
	};
	return ID_table[leg][joint];
}