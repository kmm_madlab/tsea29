/*
 * sensor_module.c
 *
 *	Created: 11/8/2016 10:04:00 AM
 *  Author: ottbe754
 */ 

#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <math.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <stdbool.h>

/*
*	States for the SPI interrupt.
*	ERR: Error.
*	IDLE: Waiting for message to start.
*	SEND: Currently sending message.
*	RECEIVING: Out of bytes to send, still receiving until master sends ETX.
*/
enum spiState {
	ERR,
	IDLE, 
	SEND, 
	RECEIVE
	};
enum spiState currentSpiState = IDLE;

/*	
*	Arrays containing the data to send with the next 
*	SPI-message and the data received in the last. 
*	Maximum size of message is 255 Bytes. 
*/
uint8_t spiSendBuffer[255] = {2,0,3};
uint8_t spiSendIndexer = 0;
uint8_t spiRecieveBuffer[255];
uint8_t spiRecieveIndexer = 0;

/*
*	These arrays and variables needs to be accessible from both the main
*	loop and the SPI interrupt
*/
int sensorSumValues[6] = {0,0,0,0,0,0};

uint8_t latestSensorValues[6] = {0,0,0,0,0,0};
uint8_t averageDistancesCm[6] = {0,0,0,0,0,0};

uint8_t averageDiv = 1;

uint8_t newSensorValue = 0;
uint8_t averageSensorValue = 0;

/*
*	These tables are used to convert the binary value from the sensors to
*	actual distance. If calibration is needed, the values in these tables
*	can be changed
*/
const int DEC_TABLE[49] = {20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,
						   50,52,54,56,58,60,62,64,66,68,70,72,74,76,78,
						   80,82,84,87,90,93,96,99,103,107,110,114,119,
						   125,131,134,139,145,149};
	
const uint8_t BIN_TABLE[49] = {0b01111101,0b01111000,0b01110001,0b01101100,
							   0b01100101,0b01100001,0b01011011,0b01010101,
							   0b01010000,0b01001110,0b01001001,0b01000100,
							   0b01000010,0b00111111,0b00111101,0b00111010,
							   0b00111001,0b00111000,0b00110101,0b00110011,
							   0b00110010,0b00110000,0b00101111,0b00101101,
							   0b00101100,0b00101011,0b00101010,0b00101001,
							   0b00101000,0b00100111,0b00100110,0b00100101,
							   0b00100100,0b00100011,0b00100010,0b00100001,
							   0b00100000,0b00011111,0b00011110,0b00011101,
							   0b00011100,0b00011011,0b00011010,0b00011001,
							   0b00011000,0b00010111,0b00010110,0b00010101,
							   0b00010100};
							   
const uint8_t SENSOR_THRESHHOLD = 50;
const uint8_t AVERAGE_THRESHHOLD = 100;

/* 
*	When spiInProgress is set to true no operations are allowed 
*	on the spiSend- and spiReciveBuffers as this could corrupt the data 
*/
bool spiInProgress = false;

/* 
*	Places the current value of averageDistances into the 
*	spiSendBuffer and formats the array so it can be sent over SPI 
*/
void spi_place_to_buffer() {
	uint8_t STX = 0x02; //ASCCI Start of TeXt
	uint8_t ETX = 0x03; //ASCCI End of TeXt
	
	spiSendBuffer[0] = STX;
	uint8_t i = 0;
	
	while (i < 6) {
		spiSendBuffer[i+1] = averageDistancesCm[i];
		i++;
	}
	spiSendBuffer[i+1] = ETX;
}

/*
* Resets all recorded sensor data 
*/
void reset_sensor_values() {
	
	for (uint8_t i = 0; i < 6; ++i) {
	sensorSumValues[i] = 0;
	latestSensorValues[i] = 0;
	averageDistancesCm[i] = 0;
	}
	
	averageDiv = 1;
	newSensorValue = 0;
	averageSensorValue = 0;
}

/* 
*	Initializes SPI Slave Device 
*/
void spi_init_slave () {
	/*	
	*	BIT7: SPI Interrupt Enable
	*	BIT6: SPI Enable
	*	BIT5: Data order, MSB first
	*	BIT4: Set as slave
	*	BIT3: Clock polarity
	*	BIT2: Clock phase
	*	BIT1-0: SPI clock divider, CPU/64 
	*/
	SPCR = 0b11000010;
	sei();
}

/*
*	Initializes the built in ADC
*/
void adc_init() {
	ADMUX = 0b01100000; //Set reference voltage to AVCC and left adjust the result in ADCL/ADCH
	ADCSRA = 0b10000111; //Enable ADC and set pre-scaler to 128 (125KHz at 16MHz system clock
}

/*
*	Initializes the IO-ports on the Atmega and sets up the registers for
*	the ADC.
*/
void init_ports() {
	/* 
	*	A7 = INPUT, Don't care.
	*	A6 = INPUT, Don't care.
	*	A5 = INPUT, Sensor 5, rear facing.
	*	A4 = INPUT, Sensor 4, front left facing
	*	A3 = INPUT, Sensor 3, rear left facing
	*	A2 = INPUT, Sensor 2, front right facing
	*	A1 = INPUT, Sensor 1, rear right facing
	*	A0 = INPUT, Sensor 0, forward facing
	*/	
	DDRA = 0x00;
	
	/* 
	*	B7 = INPUT, SPI master clock.
	*	B6 = OUTPUT, MISO.
	*	B5 = INPUT, MOSI.
	*	B4 = INPUT, SS.
	*	B3 = INPUT, Don't care.
	*	B2 = INPUT, Don't care.
	*	B1 = INPUT, Don't care.
	*	B0 = INPUT, Don't care.
	*/
	DDRB = 0b01000000;
	
	/* 
	*	D7 = OUTPUT, LED 7
	*	D6 = OUTPUT, LED 6
	*	D5 = OUTPUT, LED 5
	*	D4 = OUTPUT, LED 4
	*	D3 = OUTPUT, LED 3
	*	D2 = OUTPUT, LED 2
	*	D1 = OUTPUT, LED 1
	*	D0 = OUTPUT, LED 0
	*/	
	DDRD = 0xff;
}

/* 
*	Interrupt vector for the SPI, will trigger after a SPI-transmission
*	is completed (1 Byte). A state machine is used to know if an SPI 
*	message is currently being transmitted.
*	IDLE: Waiting for message to start.
*	SEND: Currently sending message. 
*	RECEIVING: Out of bytes to send, still receiving until master sends ETX. 
*	Upon receiving ETX, return to IDLE.
*/
ISR(SPI_STC_vect){
	PORTD |= (1<<PORTD0);
	uint8_t receivedByte = SPDR;
	uint8_t byteToSend = spiSendBuffer[spiSendIndexer];
	uint8_t STX = 0x02; //ASCCI Start of TeXt
	uint8_t ETX = 0x03; //ASCCI End of TeXt
	uint8_t NUL = 0x00; //ASCCI NULL (named NUL because NULL is reserved in math.h)
	uint8_t EOT = 0x04; //ASCCI End Of Transmission 
	
	switch (currentSpiState){
		
		case IDLE :
		if(receivedByte == NUL){
			//send start of text
			SPDR = byteToSend;
			spiSendIndexer++;
			PORTD |= (1<<PORTD7);
			currentSpiState = SEND;
			spiInProgress = true;
		}
		else{
			SPDR = EOT;
		}
		break;
		
		case SEND :
		if(byteToSend != ETX){
			//keep saving data
			spiRecieveBuffer[spiRecieveIndexer] = receivedByte;
			spiRecieveIndexer++;
			//keep sending data
			SPDR = byteToSend;
			spiSendIndexer++;
		}
		else{
			//keep saving data
			spiRecieveBuffer[spiRecieveIndexer] = receivedByte;
			spiRecieveIndexer++;
			//send ETX
			SPDR = byteToSend;
			PORTD |= (1<<PORTD6);
			currentSpiState = RECEIVE;
		}
		break;
		
		case RECEIVE :
		if (receivedByte != ETX){
			//keep saving data
			spiRecieveBuffer[spiRecieveIndexer] = receivedByte;
			spiRecieveIndexer++;
			//keep sending ETX
			SPDR = byteToSend;
		}
		else{
			//End of message, reset.
			PORTD = 0b00011000;
			SPDR = NUL;
			spiSendIndexer = 0;
			spiRecieveIndexer = 0;
			currentSpiState = IDLE;
			//reset_sensor_values();
			spiInProgress = false;
		}
		break;
	}
}

/*
*	Converts the analog signal on port a[channel] to a 8-bit value.
*	The function actually convert the value two times, 
*	since the first conversion after a channel switch is not trustworthy.
*	:param channel: Defines witch channel (port) will be used in the conversion
*	:return: The converted binary value.
*/
uint8_t adc_convert(uint8_t channel) {
	
	channel &= 0b00000111;
	ADMUX = (0xF8 & ADMUX)|channel;
	
	cli();						//disable interrupt
	ADCSRA |= (1<<ADSC);		//Start ADC
	while(ADCSRA & (1<<ADSC));	//Wait for ADC to finish
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC));	
	
	return ADCH;				//Return converted ADC value
}

/*
*	This function sends out an 8-bit value to port d0-d7, 
*	where the LEDs are placed. The value will be displayed in 
*	standard binary notation.
*	:param sensorValue: The value to send to the LEDs.
*/
void value_to_led(uint8_t sensorValue) {
	PORTD = sensorValue; 
}

/*
*	This function convert the binary value 
*	from the ADC to an actual distance in cm.
*	:param avarageBin: The binary value to be converted
*	:return: The corresponding distance in cm.
*/
uint8_t convert_sensor_bin_to_cm(uint8_t avarageBin) {
	
	int listLenght = 48;
	int sensorCmValue = 0;
	//First handels values outside the range of the tables, then values inside the range.	
	if (avarageBin < BIN_TABLE[listLenght]) { // if greater than largest length
			sensorCmValue = DEC_TABLE[listLenght];
			return sensorCmValue;
	} else if (avarageBin > BIN_TABLE[0]) { // if smaller than shortest length
			sensorCmValue = DEC_TABLE[0];
			return sensorCmValue;
	} else {					  
		for (int lc = 0; lc < listLenght; ++lc) {
			if ((avarageBin <= BIN_TABLE[lc]) 
				&& (avarageBin > BIN_TABLE[lc + 1])) {
				sensorCmValue = DEC_TABLE[lc];
				break;
			}
		}
	}
	return sensorCmValue;
}

/*
*	The main loop of the sensor module.
*	The main loop converts the values from the sensors and creates an
*	average of values measured from every sensor. Unreasonable changes
*	between two measurements are discarded. The current average of all
*	sensor-readings are then placed in the SPIsendBuffer. If the average
*	consists of too old values the average are discarded and a new 
*	average is created from new values.
*/
int main(void) {
	
	init_ports();	
	adc_init();
	spi_init_slave();
		
	//Show that init is complete on LEDs
	value_to_led(0xff);
	_delay_ms(500);
	value_to_led(0x00);

	uint8_t sensorCompareValue = 0;
		
	while (1) {
		
		for (int lc = 0; lc < 6; ++lc) {
			
			newSensorValue = adc_convert(lc);
			if (newSensorValue >= latestSensorValues[lc]){
				sensorCompareValue = newSensorValue - latestSensorValues[lc];
			} else if (latestSensorValues[lc] < newSensorValue) {
				sensorCompareValue = latestSensorValues[lc] - newSensorValue;
			}
			
			if ((sensorCompareValue > SENSOR_THRESHHOLD) 
				&& (latestSensorValues[lc] != 0)) {
				newSensorValue = latestSensorValues[lc];
			} else {
				latestSensorValues[lc] = newSensorValue;
			}
			
			cli(); //disable interrupts
			sensorSumValues[lc] += newSensorValue;
			averageSensorValue = sensorSumValues[lc]/averageDiv;
			sei();//enable interrupts
			averageDistancesCm[lc] = convert_sensor_bin_to_cm(averageSensorValue);//BEFORE averageSensorValue
		}
		++averageDiv;			
		if (averageDiv > AVERAGE_THRESHHOLD) {
			if (spiInProgress == false) {
				cli();//disable interrupt
				spi_place_to_buffer();
				sei();//enable interrupt
			}
			averageDiv = 1;
			for (int lc = 0; lc < 6; ++lc) {
				sensorSumValues[lc] = 0;
				latestSensorValues[lc] = 0;		
			}
		}	
	}
}

