"""
The server that serves the computer controlling the robot with status data and receives commands.
"""
import bluetooth
import json
import netifaces as ni

host = 'B8:27:EB:1D:71:40'  # Raspberry Pi's Mac address.
port = 3  # Port of our choosing client and server must have the same.
backlog = 1
size = 1024
auto = False


def status():
    pass  # All commands returns the status so here we do nothing.


command_actions = {'status': status}


def generate_status():
    return json.dumps({'ip': [ni.ifaddresses('wlan0')[ni.AF_INET][0]['addr']]})


def run_server():
    # Start server
    print("Bluetooth server initializing")
    bluetooth_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    bluetooth_socket.bind((host, port))
    bluetooth_socket.listen(backlog)
    print("Bluetooth server started")
    
    # Interact with client
    try:
        
        # Connect to client
        client, clientInfo = bluetooth_socket.accept()
        
        # Exchange data
        while True:
            
            data_part = client.recv(size)
            
            if data_part:
                
                data = ""
                data_part = data_part.decode('utf-8')
                data += data_part
                
                # Receive until end of message. (Doing it this way will result in bug where the program will recieve
                # forever. When a message is exactly 1008*n bites big, where n is a integer.)
                while len(data_part) == 1008:
                    data_part = client.recv(size)
                    data_part = data_part.decode('utf-8')
                    data += data_part
                # Print data received from client and send response
                client.send(generate_status())
                command = json.loads(data)
                print(command)
    except bluetooth.btcommon.BluetoothError as e:
        # If something goes wrong with bluetooth, print error, kill everything and restart.
        print(e)
        client.close()
        bluetooth_socket.close()
        print("Restarting bluetooth server")
        run_server()


if __name__ == "__main__":
    run_server()