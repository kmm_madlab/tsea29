"""
Basic server that waits for a client then just echos back whatever the client sends.
"""

import bluetooth

pi_mac_address = 'B8:27:EB:1D:71:40'
port = 3
backlog = 1
size = 1024
commands = ['status']


def run_server():
    # Starting server
    bluetooth_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    bluetooth_socket.bind((pi_mac_address, port))
    bluetooth_socket.listen(backlog)
    
    # Receive data from clients and echo back
    try:
        client, clientInfo = bluetooth_socket.accept()
        
        while True:
            data = client.recv(size)
            if data:
                print(len(data.decode('utf-8')))
                client.send(data)
    except Exception as e:
        # Close and exit on exceptions
        print(e)
        client.close()
        bluetooth_socket.close()


if __name__ == "__main__":
    run_server()