"""
Configuration file for the communication module on the Raspberry Pi.
"""
import paths
import datetime
import logging
import json
import os

INITIALIZED = False  # Have the communication been initialized
LOG_LEVEL = logging.INFO

# ----------------------------------- Path config ------------------------------ #

MODULE_PATH = paths.MODULE_PATH

# JSON file configuration
JSON_DIR = paths.JSON_DIR
JSON_STATUS_FILENAME = 'status.json'
JSON_COMMANDS_FILENAME = 'commands.json'

# Log file configuration
LOG_DIR = paths.LOG_DIR
SPI_LOG_FILENAME = 'spi.log'
BLUETOOTH_LOG_FILENAME = 'bluetooth.log'
COMMUNICATION_MODULE_LOG_FILENAME = 'communication_module.log'

# ----------------------------------- SPI config ------------------------------- #

SPI_RUN = True

# Basic SPI setup
MAX_SPEED_HZ = 250000  # Clock speed is atmega sysclock (16MHz) divided by 64 (16 000 000/64 = 250 000) is  250 KHz.
MODE = 0b00  # Clock phase settings must be the same as CPOL|CPHA on the atmega1284p cards.
CONTROL_MODULE = 0
SENSOR_MODULE = 1
PORT = 0  # SPI port only used within the Raspberry Pi. It's for if you have more than two slaves.

# ASCII
STX = 0x02  # ASCII Start of text
ETX = 0x03  # ASCII End of text
NULL = 0x00  # ASCII NULL
EOT = 0x04  # ASCII End of transmission

# Update time configuration
UPDATE_TIME = 0.05  # Amount of seconds to wait between message exchanges between the modules
SLAVE_WAIT_TIME = 0.001  # Time to wait between spi messages.


# Standard messages
GET_SENSOR_DATA = [0x48, 0x61, 0x6D, 0x70, 0x75, 0x73]

# Testing variables
TEST_SEQUENCE_ONE = [0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB]

COMMAND_TO_BYTE = {"curve_left": 57, "curve_right": 56, "stop": 55, "backward": 54, "left": 53, "right": 52, "rotate_left": 51, "rotate_right": 50,
                   "forward": 49}

MODE_TO_BYTE = {"auto": 49, "manual": 50, "config": 51}

CONFIG_MODE_TO_BYTE = {"init": 49, "speed": 50}

# --------------------------- Bluetooth config --------------------------- #

DEV_PI_HOST = 'B8:27:EB:1D:71:40'  # Hampus Raspberry Pi's Mac address.
ROBOT_PI_HOST = 'B8:27:EB:C8:28:12'  # Robot's Raspberry Pi's Mac address.
# Hampus Raspberry Pi's Mac address as an int two now, it changed once...
DEV_PI_MACS = [202481600728767, 202481597930474]
BLUETOOTH_PORT = 3  # Port of our choosing client and server must have the same.
BACKLOG = 1
MESSAGE_SIZE = 1024
BLUETOOTH_TIMEOUT_TIME = 10
BLUETOOTH_BOOT_DELAY = 5

CONTROL_MODULE_DEFAULT_CONFIG = {
    "ideal dist": 32,
    "proportional": 100,
    "break of dist": 149,
    "dist scalar": 125,
    "angle scalar": 55
}

DEFAULT_STATUS_MESSAGE = {
    "sensors": {"front": 10, "right_back": 11, "right_front": 12, "left_back": 13, "left_front": 14, "back": 15},
    "current_command": ["stop"],
    "auto": False,
    "ip": "No wifi"
}

DEFAULT_COMMAND_MESSAGE = {
    "sensors": {"front": 10, "right_back": 11, "right_front": 12, "left_back": 13, "left_front": 14, "back": 15},
    "current_command": ["stop"],
    "auto": False,
    "config_data": CONTROL_MODULE_DEFAULT_CONFIG
}


def get_log_prefix():
    """
    Returns a log prefix containing time and date of the log entry
    :return: (String) Log prefix.
    """
    now = datetime.datetime.now()
    return "[DATE: %s/%s-%s, TIME:%s:%s:%s] " % (now.day, now.month, now.year, now.hour, now.minute, now.second)


def read_command_file():
    """
    Reads the JSON file with command data and returns the data as a dict.
    :return: (Dict) The command data.
    """
    cmd_file_path = os.path.join(JSON_DIR, JSON_COMMANDS_FILENAME)
    cmd_file = open(cmd_file_path, 'r')
    try:
        cmd_data = json.load(cmd_file)
    except ValueError:
        cmd_data = DEFAULT_COMMAND_MESSAGE
    finally:
        cmd_file.close()
    return cmd_data


def write_command_file(data):
    """
    Writes a dict to the command JSON file.
    :param data: (Dict) The command data.
    :return: (None)
    """
    cmd_file_path = os.path.join(JSON_DIR, JSON_COMMANDS_FILENAME)
    cmd_file = open(cmd_file_path, 'w')
    json.dump(data, cmd_file)
    cmd_file.close()


def read_status_file():
    """
    Reads status from JSON file.
    :return: (Dict) containing the status data.
    """
    status_file_path = os.path.join(JSON_DIR, JSON_STATUS_FILENAME)
    status_file = open(status_file_path, 'r')
    try:
        status_data = json.load(status_file)
    except ValueError:
        status_data = DEFAULT_STATUS_MESSAGE
    finally:
        status_file.close()
    return status_data


def write_status_file(data):
    """
    Writes the status to JSON file.
    :param data: (Dict) The data to be written.
    :return: (None)
    """
    status_file_path = os.path.join(JSON_DIR, JSON_STATUS_FILENAME)
    status_file = open(status_file_path, 'w')
    json.dump(data, status_file)
    status_file.close()


def verify_config(config):
    """
    Verifies that no config value is a predefined spi value or bigger than one byte.
    :param config: (Dict) The configuration for the control module
    :return: (None)
    """
    min_value = 15
    max_value = 255
    for key in config:
        assert min_value < config[key] < max_value
