"""
Main file that is runned when you run the bluetooth module. It runs the bluetooth server and logs errors if they occur
"""
from bluetooth_server.bluetooth_server import main
from communication_module import config
import logging

if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logging.error(config.get_log_prefix() + str(e))

