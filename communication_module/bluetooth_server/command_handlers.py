"""
All the functions that handles commands sent over bluetooth are stored here.
"""

import communication_module.config as config
import logging


def status(args):
    pass  # All commands returns the status so here we do nothing.


def walk(args):
    """
    Handles a walk command by checking user input and writing current command to JSON file.
    :param args: (List) The first elem in the list is direction of walk that is the only used argument.
    :return: (None)
    """
    # Checking user input.
    assert isinstance(args, list)
    directions = ["right", "forward", "backward", "left", "stop", "rotate_left", "rotate_right", "curve_right", "curve_left"]
    if not (len(args) >= 1 and args[0] in directions):
        raise ValueError("Invalid argument")
    
    # Extracts direction and logs it.
    direction = args[0]
    logging.info(config.get_log_prefix() + "Walk " + direction)
    
    # Reads file JSON content.
    cmd_json = config.read_command_file()
    # Always write the current command.
    cmd_json["current_command"] = [direction]
    # Writes JSON data to file.
    config.write_command_file(cmd_json)


def toggle_auto(args):
    """
    Toggles automatic mode on/off.
    :param args: (Boolean) True if auto should be on else false.
    :return:
    """
    command_data = config.read_command_file()
    command_data['auto'] = args
    command_data['current_command'] = ["stop"]
    config.write_command_file(command_data)


def configure(args):
    """
    Handles the config command from the computer and writes the new configuration to the command file so it can be sent
    to the control module.
    :param args: (List) List of ints representing the new configuration
    :return: (None)
    """
    if len(args) != 5:
        logging.error(config.get_log_prefix() + "Configuration data not of correct length config canceled")
    for arg in args:
        if not isinstance(arg, int) and 15 < arg < 255:
            logging.error(config.get_log_prefix() + "Configuration data not of correct type and size.")
    command_data = config.read_command_file()
    config_data = command_data['config_data']
    config_data['break of dist'] = args[0]
    config_data['proportional'] = args[1]
    config_data['ideal dist'] = args[2]
    config_data['dist scalar'] = args[3]
    config_data['angle scalar'] = args[4]
    config.write_command_file(command_data)
