"""
The server that serves the computer controlling the robot with status data and receives commands.
"""
import os
import bluetooth
import json
import netifaces as ni
import logging
from uuid import getnode as get_mac
import time
import communication_module.config as config
from communication_module.bluetooth_server.command_handlers import status, walk, toggle_auto, configure

# Handlers for the different commands that the server can handle.
COMMAND_ACTIONS = {'status': status, 'walk': walk, 'auto': toggle_auto, 'config': configure}


def init():
    """
    Initialize the bluetooth server by turning on bluetooth and resolving the hostname.
    :return: (String) The servers hostname.
    """
    # Stop robot while waiting for initialization
    walk(['stop'])
    # Initialize Bluetooth
    logging.info(config.get_log_prefix() + 'Bluetooth waiting for everything to boot')
    time.sleep(config.BLUETOOTH_BOOT_DELAY)
    logging.info(config.get_log_prefix() + 'Starting bluetooth')
    bluetooth_start_command = 'sudo hciconfig hci0 piscan'
    return_code = os.system(bluetooth_start_command)  # Turns bluetooth viability on.
    if return_code:
        logging.info(config.get_log_prefix() + 'bluetooth startup error')
    else:
        logging.info(config.get_log_prefix() + 'bluetooth up and running')
        
    # Initialize server
    if not os.path.exists(config.JSON_DIR):
        os.makedirs(config.JSON_DIR)
    logging.info(config.get_log_prefix() + "Bluetooth server initializing")
    if get_mac() in config.DEV_PI_MACS:
        hostname = config.DEV_PI_HOST
    else:
        hostname = config.ROBOT_PI_HOST
    return hostname


def generate_status():
    """
    Extrats status of robot and returns it as a JSON string.
    :return: (String) Status in JSON dict.
    """
    # Reads Raspberry pi's wireless network IP addres.
    try:
        ip = ni.ifaddresses('wlan0')[ni.AF_INET][0]['addr']
    except KeyError:
        ip = "Wifi is down"
    # Reads queued commands from JSON file.
    json_cmd_path = os.path.join(config.JSON_DIR, config.JSON_COMMANDS_FILENAME)
    json_cmd_file = open(json_cmd_path, 'r')
    command = json.load(json_cmd_file)
    current_command = command["current_command"]
    auto = command['auto']
    json_cmd_file.close()
    robot_status = config.read_status_file()
    robot_status['ip'] = [ip]
    robot_status['current_command'] = current_command
    robot_status['auto'] = auto
    json_string = json.dumps(robot_status)
    return json_string


def receive_data(client):
    """
    Receives data from a client, handles errors and timeouts.
    :param client: (Bluetooth Client object) The client to receive data from.
    :return: (String) Data received from the client.
    """
    data = 0
    try:
        data = client.recv(config.MESSAGE_SIZE)
    except bluetooth.btcommon.BluetoothError as e:
        logging.warning(config.get_log_prefix() + str(e))
        client.close()
    return data


def wait_for_client(hostname):
    """
    Waits for a client and when a client has connected it returns the client and info about it. Handles timeouts during
    the wait time.
    :param hostname: (String) The servers hostname which it should listen to.
    :return: (Tupel) Tupel with the bluetooth client object and information about it.
    """
    bluetooth_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    bluetooth_socket.settimeout(config.BLUETOOTH_TIMEOUT_TIME)
    bluetooth_socket.bind((hostname, config.BLUETOOTH_PORT))
    bluetooth_socket.listen(config.BACKLOG)
    
    connected = False
    while not connected:
        connected = True
        
        try:
            # Connect to client
            client, client_info = bluetooth_socket.accept()
            logging.info(config.get_log_prefix() + "Client: {} connected!".format(client_info))
        except bluetooth.btcommon.BluetoothError as e:
            # If something goes wrong with bluetooth, print error, kill everything and restart.
            logging.warning(config.get_log_prefix() + "Waiting for client: " + str(e))
            bluetooth_socket.close()
            bluetooth_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
            bluetooth_socket.settimeout(config.BLUETOOTH_TIMEOUT_TIME)
            bluetooth_socket.bind((hostname, config.BLUETOOTH_PORT))
            bluetooth_socket.listen(config.BACKLOG)
            connected = False
            
    return client, client_info


def execute_commands(commands):
    """
    Executes commands received from the client.
    :param commands: (Dict) Dictionary with commands as keys and a list of arguments as values.
    :return: (None)
    """
    for command in commands:
        if command in COMMAND_ACTIONS:
            try:
                func = COMMAND_ACTIONS[command]
                args = commands[command]
                func(args)
            except Exception as e:
                # Prevents server from crashing from badly implemented command.
                logging.error(config.get_log_prefix() + "Error while handling command, error message: " +
                              str(e))
        else:
            logging.error(config.get_log_prefix() + "Command: %s was not found" % command)


def run_server(hostname):
    """
    Runs the bluetooth server that communicates with the client. Can only be one client connected
    at the time. Returns 1 on server failure that couldn't be handled.
    :param hostname: (String)
    :return: (Int) Exit code 1 for failure.
    """
    # Start server
    logging.info(config.get_log_prefix() + "Bluetooth server started")

    # Interact with client
    client, client_info = wait_for_client(hostname)
    
    # Exchange data
    while True:
        data_part = receive_data(client)
        if data_part and data_part != 1:
            data = ""
            data_part = data_part.decode('utf-8')
            data += data_part
            while len(data_part) == config.MESSAGE_SIZE-16:
                data_part = receive_data(client)
                data_part = data_part.decode('utf-8')
                data += data_part
                # Receive until end of message.
                while len(data_part) == config.MESSAGE_SIZE-16:
                    data_part = client.recv(config.MESSAGE_SIZE)
                    data_part = data_part.decode('utf-8')
                    data += data_part

                # Logs data received from client and send response
                client.send(generate_status())
                commands = json.loads(data)
                logging.debug(config.get_log_prefix() + str(commands))

                # Executes commands recived from client
                for command in commands:
                    if command in COMMAND_ACTIONS:
                        try:
                            func = COMMAND_ACTIONS[command]
                            args = commands[command]
                            func(args)
                        except Exception as e:
                            # Prevents server from crashing from badly implemented command.
                            logging.error(config.get_log_prefix() + "Error while handling command, error message: " +
                                          str(e))
                    else:
                        logging.error(config.get_log_prefix() + "Command: %s was not found" % command)

            # Logs data received from client and send response
            client.send(generate_status())
            commands = json.loads(data)
            logging.debug(config.get_log_prefix() + str(commands))

            # Executes commands recived from client
            execute_commands(commands)
        else:
            return 1


def main():
    """
    Main function that initializes and runs the bluetooth servers that receives commands and executes them. If it fails
    which can happen when a client disconnects or something goes wrong with the bluetooth transmission it restarts the
    server so that a new connection can be made.
    :return: None
    """
    try:
        hostname = init()
        exit_code = run_server(hostname)
        logging.debug(config.get_log_prefix() + "Bluetooth server exited with exit code: {}".format(exit_code))
        while exit_code == 1:
            logging.info(config.get_log_prefix() + "Restarting bluetooth server")
            run_server(hostname)
            exit_code = run_server(hostname)
        logging.info(config.get_log_prefix() + "Exiting bluetooth server")
    except Exception:
        # If anything goes wrong, stop robot and log error.
        walk(['stop'])
        logging.exception(config.get_log_prefix() + "Bluetooth server crashed: ")
