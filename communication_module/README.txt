Useful bash commands for the raspberry pi

"sudo hciconfig hci0 piscan" Bluetooth visibilit in/off
"hcitools" Tools for using bluetooth from the command line
"sudo systemctl status madlab.service" Print status about our service
"sudo systemctl start madlab.service" Starts our service
"sudo systemctl stop madlab.service" Stops our service
"sudo systemctl restart madlab.service" Restarts our service, useful for when you pull new changes in the code and want them to be activated
