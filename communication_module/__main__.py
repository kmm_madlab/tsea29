"""
Starts up the daemon that starts the bluetooth and spi modules.
"""
import config
import sys

# Add module to sys path
sys.path.append(config.MODULE_PATH)

# I know this isn't best practice but this is the best way, trust me.
import daemon
import __init__

if __name__ == "__main__":
    if not config.INITIALIZED:
        __init__.init()
    daemon.run_daemon()
