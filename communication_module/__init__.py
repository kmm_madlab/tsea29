"""
This file is runned when the communication_module is imported and then it makes sure all necessary files and directories
exists.
"""
import config
import os


def create_files(directory, filenames):
    """
    Creates all the files in the specified directory.
    :param directory: (String) Path to the directory in which the files should be created.
    :param filenames: (List) List of strings containing the filenames of the files to be created.
    :return:
    """
    for filename in filenames:
        path = os.path.join(directory, filename)
        if not os.path.exists(path):
            f = open(path, "w")
            f.close()


def init():
    """
    Make sure needed directories and files exists
    """
    for path in [config.LOG_DIR, config.JSON_DIR]:
        if not os.path.exists(path):
            os.makedirs(path)
    
    log_files = [config.SPI_LOG_FILENAME, config.BLUETOOTH_LOG_FILENAME, config.COMMUNICATION_MODULE_LOG_FILENAME]
    create_files(config.LOG_DIR, log_files)
    
    json_files = [config.JSON_COMMANDS_FILENAME, config.JSON_STATUS_FILENAME]
    create_files(config.JSON_DIR, json_files)
    command_data = config.DEFAULT_COMMAND_MESSAGE
    config.write_command_file(command_data)

if __name__ == "communication_module":
    init()
