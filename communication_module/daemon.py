"""
The daemon that creates processes that runs the bluetooth server and spi module.
"""
from multiprocessing import Process
import os
import logging
import config
import bluetooth_server.bluetooth_server as bluetooth_server
import spi_module.high_level as spi_module
import time
from gpiozero import Button


def auto_button_cb():
    """
    Toggle the mode if it's set to auto it is toggle to manuel and current command is set to stop otherwise auto is
    turned on.
    :return: (None)
    """
    command = config.read_command_file()
    if command['auto']:
        command['auto'] = False
        command['current_command'] = ['stop']
    else:
        command['auto'] = True
    config.write_command_file(command)


def run_daemon():
    """
    Spawns two processes that runs the spi and bluetooth, also logs important information about the processes.
    :return: (None)
    """
    log_file = os.path.join(config.LOG_DIR, config.COMMUNICATION_MODULE_LOG_FILENAME)
    logging.basicConfig(filename=log_file, level=config.LOG_LEVEL)
    
    button = Button(5, pull_up=False)
    button.when_pressed = auto_button_cb
    
    if config.SPI_RUN:
        # Create the SPI process
        spi_process = Process(target=spi_module.main, name="madlab_spi")
        spi_process.start()
        log_message = "SPI daemon started with the pid: {0}, name: {1}, alive: {2}"
        log_message = log_message.format(spi_process.pid, spi_process.name, spi_process.is_alive())
        logging.info(config.get_log_prefix() + log_message)
    else:
        logging.warning(config.get_log_prefix() + "SPI is turned off")
    
    # Create the bluetooth process
    bluetooth_process = Process(target=bluetooth_server.main, name="madlab_bluetooth")
    bluetooth_process.start()
    log_message = "Bluetooth server started with the pid: {0}, name: {1}, alive: {2}"
    log_message = log_message.format(bluetooth_process.pid, bluetooth_process.name, bluetooth_process.is_alive())
    logging.info(config.get_log_prefix() + log_message)
    

    while True:
        if config.SPI_RUN:
            logging.debug(config.get_log_prefix() + "SPI alive: {}".format(spi_process.is_alive()))
        logging.debug(config.get_log_prefix() + "Bluetooth alive: {}".format(bluetooth_process.is_alive()))
        time.sleep(20)
