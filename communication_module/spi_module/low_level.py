"""
This file contains functions for sending and handling messages over SPI and some basic functions for handling bytes.
"""
import spidev
import time
import communication_module.config as config
import logging


def init_spi(module):
    """
    Initialize SPI for a specified chip select.
    :param module: (Int) Chip select, selects what slave module to talk to
    :return: (spidev.SpiDev) spi object used to communicate over SPI.
    """
    # Init SPI for the specific device
    spi = spidev.SpiDev()
    spi.open(config.PORT, module)
    spi.max_speed_hz = config.MAX_SPEED_HZ
    spi.mode = config.MODE
    return spi


def send(message, module, delay):
    """
    Test function, sends a message every second and print the response.
    :param message: (List) List of bytes that are sent to the slave device each second.
    :param module: (Int) The slave module to send the message to.
    :param delay: (Int) Delay time between bytes in seconds
    :return: (None) Instead of return ing it s the response.
    """
    spi = init_spi(module)
    
    try:
        while True:
            response = []
            for byte in message:
                response.append(spi.xfer(byte))
                time.sleep(delay)
            print(response)
            time.sleep(1)
    except Exception as e:
        print(e)
    finally:
        spi.close()


def exchange_message(message, module, wait=True):
    """
    Exchanges a list of bytes with the selected device. Exchange continues until both parts sent their ETX.
    :param message: (List) List of bytes representing the message to the slave.
    :param module: (Int) The CS (SS) to choose control or sensor module.
    :param wait: (Boolean) Should the master wait for slave to respond true for yes false for no.
    :return: (List) List of bytes exchanged with the slave.
    """
    message_max_length = 200
    # Make sure data starts with STX and ends with ETX
    if message[0] != config.STX:
        message = [config.STX] + message
    if message[-1] != config.ETX:
        message += [config.ETX]
        
    if len(message) > message_max_length:
        raise OverflowError("SPI message to long can't be longer than {}".format(message_max_length))
    if config.NULL in message:
        raise ValueError("Message can not contain NULL (0x00)")
        
    # Initialization
    spi = init_spi(module)
    response = list()
    
    try:
        # Stat: WAIT, will wait for slave to respond and make sure it's ready to exchange message
        logging.debug(config.get_log_prefix() + "Waiting")
        byte = spi.xfer([config.NULL])
        while byte[0] != config.STX and wait:
            time.sleep(config.SLAVE_WAIT_TIME)
            byte = spi.xfer([config.NULL])
            time.sleep(config.SLAVE_WAIT_TIME)
            logging.debug(config.get_log_prefix() + str(byte))
        # State: SEND, will send message until done and saving the response.
        logging.debug(config.get_log_prefix() + "Sending")
        while message:
            # Exchange next byte
            byte = spi.xfer([message[0]])
            time.sleep(config.SLAVE_WAIT_TIME)
            message = message[1:]
            response += byte
        # State: RECEIVE, will receive and save message from slave until it is done while saving.
        logging.debug(config.get_log_prefix() + "Receiving")
        byte = spi.xfer([config.ETX])
        time.sleep(config.SLAVE_WAIT_TIME)
        while byte[0] in [config.ETX, config.EOT]:
            logging.debug(config.get_log_prefix() + str(byte))
            response += byte
            byte = spi.xfer([config.ETX])
            time.sleep(config.SLAVE_WAIT_TIME)
        logging.debug(config.get_log_prefix() + "Done")
    except Exception as e:
        logging.error(config.get_log_prefix() + str(e))
    finally:
        spi.close()  # Make sure we clean up even if something crashes and burns
        
    # Clean up the response, convert it to string and return it as a JSON string.
    logging.debug(config.get_log_prefix() + str(response))
    response = clean(response)
    logging.debug(config.get_log_prefix() + str(response))
    return response


def clean(ascii_bytes):
    """
    Takes a list of bytes interprets it as ASCII character and strips it from unnecessary characters.
    :param ascii_bytes: (List) List of bytes to be striped.
    :return: (List) Cleaned list of bytes.
    """
    res = list()
    for byte in ascii_bytes:
        if byte not in [config.NULL, config.STX, config.ETX, config.EOT]:
            res.append(byte)
    return res


def bytes_to_string(ascii_bytes):
    """
    Takes a lists of bytes and interprets it as a list of ASCII character that is returned as a string.
    :param ascii_bytes: (List) The list of bytes to be interpret
    :return: (String) A string created from the bytes.
    """
    res = ""
    for byte in ascii_bytes:
        res += chr(byte)
    return res


def reverse_bits(byte):
    """
    Reverse a byte making LSB to MSB and vice verse so 10101111 will be 11110101
    :param byte: (Byte) takes exactly 8 bites in the form 0bXXXXXXXX where X is 1 or 0.
    :return: (Int) of the byte in reverse
    """
    byte = ((byte & 0xF0) >> 4) | ((byte & 0x0F) << 4)
    byte = ((byte & 0xCC) >> 2) | ((byte & 0x33) << 2)
    byte = ((byte & 0xAA) >> 1) | ((byte & 0x55) << 1)
    return byte


def bytes_to_hex(bytes_to_convert):
    """
    Takes a list of bytes and returns a list of hex
    :param bytes_to_convert: (List) List of bytes in the form of 0bXXX...X where X is 1 or 0
    :return: (List) List of hex on the form of 0xXXX...X where X is a hex (0-F).
    """
    return ''.join(["0x%02X " % x for x in bytes_to_convert]).strip()


def message_to_bytes(message):
    """
    Converts a message to a list of bytes
    :param message: (String) The message that is to be converted
    :return: (List) List of bytes where each byte is a int.
    """
    byte_list = list()
    for ele in message:
        byte_list.append(ord(ele))
    return byte_list
