"""
A daemon to control SPI handling communication between modules within the robot. This file is supposed to run in the
background and it's behaviour can be changed using the config.py file.
"""
import communication_module.config as config
import communication_module.spi_module.low_level as spi
import time
import json
import os
import logging

CURRENT_CM_CONFIG = False


def init():
    """
    Initialize the daemon so it's ready
    :return: (None)
    """
    # Create directories
    for directory in [config.JSON_DIR, config.LOG_DIR]:
        if not os.path.exists(directory):
            os.makedirs(directory)


def read_sensor_module():
    """
    Reads sensor data from the sensor module over SPI and write them to file as a JSON string
    :return: (None)
    """
    # Exchange the query for sensor data.
    response = spi.exchange_message(config.GET_SENSOR_DATA, config.SENSOR_MODULE)
    logging.debug(config.get_log_prefix() + "Read sensor data: {}".format(response))
    if len(response) != 6:
        logging.error(config.get_log_prefix() + "SPI response from sensor module not of standard length")
        return
    
    # Read in current status
    status = config.read_status_file()
    
    # Update status
    status['sensors'] = {'front': response[0],
                         'right_back': response[1],
                         'right_front': response[2],
                         'left_back': response[3],
                         'left_front': response[4],
                         'back': response[5]}
    
    # Write status
    config.write_status_file(status)
    

def cm_generate_config_message(config_data):
    """
    Formats the SPI message to be sent to the control module.
    :param config_data: (Dict) The configuration data that is to be sent.
    :return: (List) List of bytes that is the SPI message.
    """
    message = [
        config.STX,
        config.MODE_TO_BYTE['config'],
        config.CONFIG_MODE_TO_BYTE['init'],
        config_data['break of dist'],
        config_data['proportional'],
        config_data['ideal dist'],
        config_data["dist scalar"],
        config_data["angle scalar"],
        config.ETX
    ]
    return message


def cm_generate_auto_message():
    """
    Generates the message for the control module when the robot is in auto mode.
    :return: (List) The message to be sent over SPI as a list of bytes.
    """
    status = config.read_status_file()
    sensors = status['sensors']
    message = [
        config.STX,
        config.MODE_TO_BYTE['auto'],
        sensors['front'],
        sensors['right_back'],
        sensors['right_front'],
        sensors['left_back'],
        sensors['left_front'],
        sensors['back'],
        config.ETX
    ]
    return message


def cm_generate_manuel_message(stop=False):
    """
    Generates the command message that is used to send manual commands to the robot. Can also be used to stop the robot.
    :return: (List) list of bytes to be sent to the control module containing the mode and the current command.
    """
    if stop:
        command = "stop"
    else:
        json_cmd_path = os.path.join(config.JSON_DIR, config.JSON_COMMANDS_FILENAME)
        json_cmd_file = open(json_cmd_path, 'r')
        try:
            command = json.load(json_cmd_file)['current_command'][0]
        except ValueError:
            command = "stop"
        json_cmd_file.close()
    message = [config.STX, config.MODE_TO_BYTE['manual'], config.COMMAND_TO_BYTE[command], config.ETX]
    return message


def inform_robot():
    """
    Informs the control module about sensor values.
    :return: (None)
    """
    message = cm_generate_auto_message()
    response = spi.exchange_message(message, config.CONTROL_MODULE)
    logging.debug(response)


def command_robot():
    """
    Sends command to the control module and logs the respons
    :return: (None)
    """
    message = cm_generate_manuel_message()
    response = spi.exchange_message(message, config.CONTROL_MODULE)
    logging.debug(response)


def config_changed(config_data):
    """
    Checks if the config in the command file is different from last time we configured the control module.
    :param config_data: (Dict) The new configuration data.
    :return: (Boolean) True if the config has changed else false.
    """
    global CURRENT_CM_CONFIG
    
    if not CURRENT_CM_CONFIG:
        CURRENT_CM_CONFIG = config_data
        return True
    
    config_keys = ["ideal dist", "proportional", "break of dist", "dist scalar", "angle scalar"]
    for key in config_keys:
        if CURRENT_CM_CONFIG[key] != config_data[key]:
            CURRENT_CM_CONFIG = config_data
            return True
    
    return False


def config_robot():
    """
    Sends the configurations to the control module if there are any new configurations.
    :return: (None)
    """
    global CURRENT_CM_CONFIG
    ascii_ack = 0x06
    command_data = config.read_command_file()
    config_data = command_data['config_data']
    if config_changed(config_data):
        message = cm_generate_config_message(config_data)
        logging.info(config.get_log_prefix() + "Configuring robot with message: {}".format(message))
        response = spi.exchange_message(message, config.CONTROL_MODULE)
        while [ascii_ack, config.MODE_TO_BYTE['config']] != response:
            response = spi.exchange_message(message, config.CONTROL_MODULE)
        CURRENT_CM_CONFIG = config_data


def main():
    """
    Initialize and runs the spi. It asks for sensor values and commands or informs the robot depending on if it is in
    AUTO mode or not. Regardless of mode configuration is passed on to the control module. This is repeated
    indefinitely.
    :return: (None)
    """
    init()
    logging.info(config.get_log_prefix() + "SPI module initialized")
    while True:
        config_robot()
        logging.info(config.get_log_prefix() + "Reading sensors")
        read_sensor_module()
        command_data = config.read_command_file()
        if command_data['auto']:
            logging.info(config.get_log_prefix() + "Informing robot")
            inform_robot()
        else:
            logging.info(config.get_log_prefix() + "Commanding robot")
            command_robot()
        time.sleep(config.UPDATE_TIME)

